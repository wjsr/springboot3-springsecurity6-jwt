package com.doodle.authorize.event;

import com.doodle.system.overide.MyUserDetails;
import com.doodle.framework.common.constant.Constants;
import com.doodle.system.service.LoginLogService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.event.AbstractAuthenticationFailureEvent;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.stereotype.Component;

/**
 * 认证事件处理
 */
@Component
@RequiredArgsConstructor
public class AuthenticationEvents {
    private final LoginLogService loginLogService;

    @EventListener
    public void onSuccess(AuthenticationSuccessEvent event) {
        // 用户信息
        MyUserDetails user = (MyUserDetails) event.getAuthentication().getPrincipal();

        // 保存登录日志
        loginLogService.save(user.getUsername(), Constants.SUCCESS);
    }

    @EventListener
    public void onFailure(AbstractAuthenticationFailureEvent event) {
        // 用户名
        String username = (String) event.getAuthentication().getPrincipal();

        // 保存登录日志
        loginLogService.save(username, Constants.FAIL);
    }

}