package com.doodle.authorize.filter;


import com.doodle.system.overide.MyUserDetails;
import com.doodle.system.overide.MyUserDetailsService;
import com.doodle.framework.common.annotation.AnonApi;
import com.doodle.framework.common.utils.JwtUtils;
import com.doodle.framework.common.exception.CustomException;
import com.doodle.framework.common.exception.ErrorEnum;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerExecutionChain;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import java.io.IOException;


@Component
@RequiredArgsConstructor
@Slf4j
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private final JwtUtils jwtUtils;
    private final MyUserDetailsService myUserDetailsService;
    private final RequestMappingHandlerMapping requestMappingHandlerMapping;


    @Override
    protected void doFilterInternal(@NonNull HttpServletRequest request,
                                    @NonNull HttpServletResponse response,
                                    @NonNull FilterChain filterChain) throws ServletException, IOException {

        final String token = request.getHeader("Authorization");
        if (StringUtils.isBlank(token)) {
            // token 不存在，放行，交由 SpringSecurity 鉴权
            filterChain.doFilter(request, response);
            return;
        }


        HandlerMethod handlerMethod = getHandlerMethod(request);
        if (handlerMethod != null) {
            AnonApi anonApi = handlerMethod.getMethod().getAnnotation(AnonApi.class);
            if (anonApi != null) {
                // 公共接口，放行
                filterChain.doFilter(request, response);
                return;
            }
        }

        // 用户信息
        MyUserDetails userDetails;
        try {
            String username = jwtUtils.extractAllClaims(token).getSubject();
            userDetails = (MyUserDetails) myUserDetailsService.loadUserByUsername(username);
        } catch (Exception e) {
            throw CustomException.of(ErrorEnum.LOGIN_STATUS_INVALID);
        }

        // 用户被禁用
        if (!userDetails.isEnabled()){
            throw CustomException.of(ErrorEnum.LOGIN_STATUS_INVALID);
        }

        // 互踢
        String cacheToken = jwtUtils.getCacheToken(userDetails.getUserId());
        if (cacheToken == null || !cacheToken.equals(token)) {
            throw CustomException.of(ErrorEnum.LOGIN_STATUS_INVALID);
        }


        // 认证成功，往 Security 上下文存入用户信息
        SecurityContext context = SecurityContextHolder.createEmptyContext();
        Authentication authToken =
                new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
        context.setAuthentication(authToken);
        SecurityContextHolder.setContext(context);

        filterChain.doFilter(request, response);
    }

    private HandlerMethod getHandlerMethod(HttpServletRequest request) {
        HandlerMethod handlerMethod = null;
        try {
            HandlerExecutionChain handlerExecutionChain = requestMappingHandlerMapping.getHandler(request);
            if (handlerExecutionChain != null) {
                Object handler = handlerExecutionChain.getHandler();
                if (handler instanceof HandlerMethod) {
                    handlerMethod = (HandlerMethod) handler;
                }
            }
        } catch (Exception e) {
            log.error("HandlerMethod 获取失败", e);
        }
        return handlerMethod;
    }

}
