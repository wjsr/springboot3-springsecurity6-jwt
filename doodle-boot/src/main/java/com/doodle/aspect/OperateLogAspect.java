package com.doodle.aspect;


import com.google.common.collect.Maps;
import com.doodle.framework.common.constant.Constants;
import com.doodle.framework.common.utils.HttpContextUtils;
import com.doodle.framework.common.utils.IpUtils;
import com.doodle.framework.common.utils.JsonUtils;
import com.doodle.framework.operatelog.annotations.OperateLog;
import com.doodle.framework.security.utils.SecurityUtils;
import com.doodle.system.entity.OperateLogDO;
import com.doodle.system.mapper.OperateLogMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.web.multipart.MultipartFile;
import java.lang.annotation.Annotation;
import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.IntStream;

/**
 * 操作日志，切面处理类
 */
@Aspect
@Component
@AllArgsConstructor
public class OperateLogAspect {

    private final OperateLogMapper operateLogMapper;

    @Around("@annotation(operateLog)")
    public Object around(ProceedingJoinPoint joinPoint, OperateLog operateLog) throws Throwable {
        // 记录开始时间
        DateTime startTime = DateTime.now();
        try {
            // 执行方法
            Object result = joinPoint.proceed();

            // 保存日志
            saveLog(joinPoint, operateLog, startTime, Constants.SUCCESS, result);

            return result;
        } catch (Exception e) {
            // 保存日志
            saveLog(joinPoint, operateLog, startTime, Constants.FAIL, e.getMessage());
            throw e;
        }
    }


    private void saveLog(ProceedingJoinPoint joinPoint, OperateLog operateLog, DateTime startTime, Integer status, Object result) {
        OperateLogDO log = new OperateLogDO();

        // 执行时长
        long duration = new Duration(startTime, DateTime.now()).getMillis();
        log.setDuration(duration);
        // 操作者
        log.setUserId(SecurityUtils.getUserId());
        // 操作类型
        log.setOperateType(operateLog.type());
        // 设置 module 值
        log.setModule(operateLog.module());
        // 设置 name 值
        log.setName(operateLog.name());

        // 如果没有指定 module 值，则从 tag 读取
        if (StringUtils.isBlank(log.getModule())) {
            Tag tag = getClassAnnotation(joinPoint, Tag.class);
            if (tag != null) {
                log.setModule(tag.name());
            }
        }

        // 如果没有指定 name 值，则从 operation 读取
        if (StringUtils.isBlank(log.getName())) {
            Operation operation = getMethodAnnotation(joinPoint, Operation.class);
            if (operation != null) {
                log.setName(operation.summary());
            }
        }

        // 请求相关
        HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
        if (request != null) {
            log.setIp(IpUtils.getIpAddr(request));
            log.setAddress(IpUtils.getAddressByIP(log.getIp()));
            log.setUserAgent(request.getHeader(HttpHeaders.USER_AGENT));
            log.setReqUri(request.getRequestURI());
            log.setReqMethod(request.getMethod());
        }

        log.setReqParams(StringUtils.substring(obtainMethodArgs(joinPoint), 0, 2000));
        log.setStatus(status);
        log.setResultMsg(StringUtils.substring(JsonUtils.toJsonString(result), 0, 2000));

        // 保存操作日志
        operateLogMapper.insert(log);
    }

    private String obtainMethodArgs(ProceedingJoinPoint joinPoint) {
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        String[] argNames = methodSignature.getParameterNames();
        Object[] argValues = joinPoint.getArgs();

        // 拼接参数
        Map<String, Object> args = Maps.newHashMapWithExpectedSize(argNames.length);
        for (int i = 0; i < argNames.length; i++) {
            String argName = argNames[i];
            Object argValue = argValues[i];
            args.put(argName, ignoreArgs(argValue) ? "[ignore]" : argValue);
        }

        return JsonUtils.toJsonString(args);
    }

    private static boolean ignoreArgs(Object object) {
        if (object == null) {
            return false;
        }
        Class<?> clazz = object.getClass();

        // 处理数组
        if (clazz.isArray()) {
            return IntStream.range(0, Array.getLength(object))
                    .anyMatch(index -> ignoreArgs(Array.get(object, index)));
        }

        // 处理集合
        if (Collection.class.isAssignableFrom(clazz)) {
            return ((Collection<?>) object).stream()
                    .anyMatch((Predicate<Object>) OperateLogAspect::ignoreArgs);
        }

        // 处理Map
        if (Map.class.isAssignableFrom(clazz)) {
            return ignoreArgs(((Map<?, ?>) object).values());
        }

        return object instanceof MultipartFile
                || object instanceof HttpServletRequest
                || object instanceof HttpServletResponse
                || object instanceof BindingResult;
    }

    private static <T extends Annotation> T getMethodAnnotation(ProceedingJoinPoint joinPoint, Class<T> annotationClass) {
        return ((MethodSignature) joinPoint.getSignature()).getMethod().getAnnotation(annotationClass);
    }

    private static <T extends Annotation> T getClassAnnotation(ProceedingJoinPoint joinPoint, Class<T> annotationClass) {
        return ((MethodSignature) joinPoint.getSignature()).getMethod().getDeclaringClass().getAnnotation(annotationClass);
    }
}