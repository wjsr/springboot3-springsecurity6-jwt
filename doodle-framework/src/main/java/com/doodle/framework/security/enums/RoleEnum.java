package com.doodle.framework.security.enums;


import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;


@Getter
@AllArgsConstructor
public enum RoleEnum {

    USER(1,"user", "普通用户"),
    ADMIN(2,"admin", "管理员"),
    SUPER_ADMIN(3,"super_admin", "超级管理员"),
    ;

    @JsonValue
    @EnumValue
    private final Integer code;
    private final String value;
    private final String label;

    @JsonCreator
    public static RoleEnum of(Integer code){
        for (RoleEnum value : RoleEnum.values()){
            if (value.getCode().equals(code)){
                return value;
            }
        }
        return null;
    }



}
