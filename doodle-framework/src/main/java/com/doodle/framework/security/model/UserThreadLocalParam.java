package com.doodle.framework.security.model;


import com.doodle.framework.security.enums.AuthEnum;
import com.doodle.framework.security.enums.RoleEnum;
import lombok.Data;

import java.util.List;

/**
 * 保存用户相关参数的对象，作为 ThreadLocalMap 中的 value
 */
@Data
public class UserThreadLocalParam {
    private Long userId;
    private String username;
    private String password;
    private List<RoleEnum> roles;
    private List<AuthEnum> auths;
}
