package com.doodle.framework.security.enums;


import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum AuthEnum {

    SYSTEM_USER_ADD(1,"system:user:add", "用户添加"),
    SYSTEM_USER_DEL(2, "system:user:del", "用户删除"),
    SYSTEM_USER_EDIT(3, "system:user:edit", "用户编辑"),
    SYSTEM_ROLE_ASSIGN(4, "system:role:assign", "角色分配"),
    SYSTEM_USER_EXPORT(5, "system:user:export", "用户导出"),
    SYSTEM_ROLE_EDIT(6, "system:role:edit", "角色编辑"),
    SYSTEM_MENU_ADD(7, "system:menu:add", "菜单添加"),
    SYSTEM_MENU_EDIT(8, "system:menu:edit", "菜单编辑"),
    SYSTEM_MENU_DEL(9, "system:menu:del", "菜单删除"),
    SYSTEM_DICT_ADD(10, "system:dict:add", "字典添加"),
    SYSTEM_DICT_EDIT(11, "system:dict:edit", "字典编辑"),
    SYSTEM_DICT_DEL(12, "system:dict:del", "字典删除"),
    SYSTEM_DICT_REFRESH(13, "system:dict:refresh", "刷新字典缓存"),
    SYSTEM_LOG_DEL(14, "system:log:del", "日志删除"),
    SYSTEM_LOG_EXPORT(15, "system:log:export", "日志导出"),
    ;

    @JsonValue
    @EnumValue
    private final Integer code;
    private final String value;
    private final String label;

}
