package com.doodle.framework.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
public class DateUtils {

    // 返回当天最后时刻
    public static Date endOfDay(Date date) {
        if (date == null) {
            return null;
        }
        return new DateTime(date).withTime(23, 59, 59, 0).toDate();
    }

    // 将日期转成指定格式字符串
    public static String format(Date date, String pattern) {

        return new SimpleDateFormat(pattern).format(date);
    }

    // 将字符串转成指定格式日期
    public static Date parse(String dateString, String pattern) {
        try {
            return new SimpleDateFormat(pattern).parse(dateString);
        } catch (ParseException e) {
            log.error("日期转换失败", e);
        }
        return null;
    }
}
