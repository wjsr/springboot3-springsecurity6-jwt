package com.doodle.framework.common.utils;

import com.alibaba.excel.EasyExcel;
import com.doodle.framework.common.exception.CustomException;
import com.doodle.framework.common.exception.ErrorEnum;
import jakarta.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Slf4j
public class ExcelUtils {

    public static <T> void excelExport(Class<T> clazz, String excelName, String sheetName, List<T> data) {
        HttpServletResponse response = getExportResponse(excelName);
        try {
            EasyExcel.write(response.getOutputStream(), clazz)
                    .sheet(StringUtils.isBlank(sheetName) ? "sheet1" : sheetName)
                    .doWrite(data);
        } catch (Exception e) {
            log.error("excel导出失败", e);
            throw CustomException.of(ErrorEnum.DATA_EXPORT_ERROR);
        }
    }

    private static HttpServletResponse getExportResponse(String excelName) {
        HttpServletResponse response = HttpContextUtils.getHttpServletResponse();
        response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
        response.setCharacterEncoding("UTF-8");
        String fileName = URLEncoder.encode(excelName, StandardCharsets.UTF_8);
        response.setHeader("Content-disposition", "attachment;filename=" + fileName + ".xlsx");

        return response;
    }
}
