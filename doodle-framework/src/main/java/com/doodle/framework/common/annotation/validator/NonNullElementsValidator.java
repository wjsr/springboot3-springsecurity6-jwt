package com.doodle.framework.common.annotation.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.util.Collection;

public class NonNullElementsValidator implements ConstraintValidator<NonNullElements, Collection<?>> {
    @Override
    public void initialize(NonNullElements constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(Collection<?> list, ConstraintValidatorContext context) {

        if (list == null) {
            return true; // 空集合不进行校验
        }

        for (Object element : list) {
            if (element == null) {
                return false;
            }
        }

        return true;
    }
}
