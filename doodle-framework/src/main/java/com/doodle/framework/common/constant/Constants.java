package com.doodle.framework.common.constant;

import java.util.TimeZone;

/**
 * 常量
 */
public class Constants {

    public static final TimeZone TIME_ZONE = TimeZone.getTimeZone("GMT+8");

    public static final Integer SUCCESS = 1;
    public static final Integer FAIL = 0;

    public static final String DATE_TIME_PATTERN = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_PATTERN = "yyyy-MM-dd";

    /**
     * 验证码有效期（秒）
     */
    public static final Integer CAPTCHA_EXPIRATION = 2 * 60;

}
