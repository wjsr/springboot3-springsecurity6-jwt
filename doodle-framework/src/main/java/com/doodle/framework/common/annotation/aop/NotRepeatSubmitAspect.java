package com.doodle.framework.common.annotation.aop;



import com.doodle.framework.cache.RedisCache;
import com.doodle.framework.cache.RedisKeys;
import com.doodle.framework.common.utils.HttpContextUtils;
import com.doodle.framework.common.utils.IpUtils;
import com.doodle.framework.common.exception.CustomException;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;


@Aspect
@Component
@RequiredArgsConstructor
public class NotRepeatSubmitAspect {

    private final RedisCache redisCache;

    /**
     * 判断重复提交的依据是 user_ip:uri:args
     */
    @Before("@annotation(notRepeatSubmit)")
    public void repeatSubmitCheck(JoinPoint jp, NotRepeatSubmit notRepeatSubmit) {

        HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(IpUtils.getIpAddr(request));
        stringBuilder.append(":");
        stringBuilder.append(request == null ? "unknown" : request.getRequestURI());
        stringBuilder.append(":");
        for (Object arg : jp.getArgs()) {
            if (arg != null) {
                stringBuilder.append(arg);
            }
        }
        int timeout = notRepeatSubmit.interval();
        String key = RedisKeys.getRepeatSubmitKey(stringBuilder);
        if (!redisCache.getLock(key, timeout)) {
            throw CustomException.of("正在处理，禁止重复提交");
        }
    }
}
