package com.doodle.framework.common.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

/**
 * 分页工具类
 */
@Data
@Schema(description = "分页数据")
public class PageResult<T> implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    @Schema(description = "总记录数")
    private long total;

    @Schema(description = "列表数据")
    private List<T> rows;

    public PageResult(List<T> list, long total) {
        this.rows = list;
        this.total = total;
    }
}