package com.doodle.framework.common.annotation.validator;


import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = NonNullElementsValidator.class)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
public @interface NonNullElements {

    String message() default "集合元素不能为空";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
