package com.doodle.framework.common.utils;


import java.util.Random;


public class FakerUtils {


    public static String generateRandomNickname(int length) {
        // 可选择的字符集
        String charSet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()-=_+[]{}|;:,.<>?";
        Random random = new Random();
        StringBuilder nickname = new StringBuilder(length);

        // 随机生成昵称
        for (int i = 0; i < length; i++) {
            int randomIndex = random.nextInt(charSet.length());  // 获取字符集中的一个随机字符
            nickname.append(charSet.charAt(randomIndex));
        }

        return nickname.toString();
    }
}
