package com.doodle.framework.common.utils;


import com.doodle.framework.cache.RedisCache;
import com.doodle.framework.cache.RedisKeys;
import com.doodle.framework.common.exception.CustomException;
import io.jsonwebtoken.*;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import java.security.Key;
import java.util.Date;
import java.util.HashMap;

@Component
@RequiredArgsConstructor
public class JwtUtils {

    private final RedisCache redisCache;

    /**
     * 密钥
     */
    @Value("${token.secret}")
    private String secret;
    /**
     * 有效时间
     */
    @Value("#{${token.expiration}}")
    private long expiration;

    /**
     * 生成 token
     */
    public String generateToken(String username, Long userId) {
        HashMap<String, Object> extraClaims = new HashMap<>();

        String token = Jwts.builder().setClaims(extraClaims).setSubject(username)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + expiration))
                .signWith(getSigningKey(), SignatureAlgorithm.HS256).compact();

        redisCache.set(RedisKeys.getAccessTokenKey(userId), token, expiration / 1000);

        return token;
    }

    public String getCacheToken(Long userId) {
        return (String) redisCache.get(RedisKeys.getAccessTokenKey(userId));
    }

    public void delCacheToken(Long userId) {
        redisCache.delete(RedisKeys.getAccessTokenKey(userId));
    }

    /**
     * 获取 token body
     */
    public Claims extractAllClaims(String token) {
        Claims claims;
        try {
            claims = Jwts.parserBuilder().setSigningKey(getSigningKey()).build().parseClaimsJws(token)
                    .getBody();
        } catch (ExpiredJwtException e){
            throw CustomException.of("过期 token");
        } catch (JwtException e){
            throw CustomException.of("非法 token");
        }
        return claims;
    }

    private Key getSigningKey() {
        byte[] keyBytes = Decoders.BASE64.decode(secret);
        return Keys.hmacShaKeyFor(keyBytes);
    }
}
