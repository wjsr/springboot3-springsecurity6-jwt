package com.doodle.framework.mybatis.service.impl;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.doodle.framework.common.model.PageParam;
import com.doodle.framework.mybatis.service.BaseService;


public class BaseServiceImpl<M extends BaseMapper<T>, T> extends ServiceImpl<M, T> implements BaseService<T> {

    /**
     * 获取分页对象
     */
    protected IPage<T> getPage(PageParam pageParam) {

        return new Page<>(pageParam.getPageNum(), pageParam.getPageSize());
    }
}