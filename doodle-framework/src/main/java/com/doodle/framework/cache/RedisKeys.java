package com.doodle.framework.cache;

public class RedisKeys {

    private static final String ACCESS_TOKEN = "access_token:%s";
    private static final String REPEAT_SUBMIT = "repeat_submit:%s";
    private static final String CAPTCHA_CODE_KEY = "captcha_codes:%s";

    public static String getAccessTokenKey(Long userId){
        return String.format(ACCESS_TOKEN, userId);
    }

    public static String getRepeatSubmitKey(StringBuilder builder){
        return String.format(REPEAT_SUBMIT, builder);
    }

    public static String getCaptchaCodeKey(String uuid) {
        return String.format(CAPTCHA_CODE_KEY, uuid);
    }

}
