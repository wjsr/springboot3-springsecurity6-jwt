package com.doodle.framework.storage.enums;

/**
 * 存储类型
 */
public enum StorageTypeEnum {
    /**
     * 本地
     */
    LOCAL,
    /**
     * 阿里云
     */
    ALIYUN,
}
