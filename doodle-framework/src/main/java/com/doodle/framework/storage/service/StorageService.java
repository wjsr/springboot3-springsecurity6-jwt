package com.doodle.framework.storage.service;

import com.doodle.framework.storage.properties.StorageProperties;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import java.io.InputStream;

/**
 * 对象存储
 */
public abstract class StorageService {

    public StorageProperties properties;

    private String getPath() {
        // 文件路径
        String path = DateTime.now().toString("yyyyMMdd");

        // 如果有前缀，则也带上
        if (StringUtils.isNotBlank(properties.getConfig().getPrefix())) {
            path = properties.getConfig().getPrefix() + "/" + path;
        }

        return path;
    }

    private String getNewFileName(String fileName) {
        // 当天时间戳
        long time = DateTime.now().getMillis();

        // 新文件名
        return time + "_" + fileName;
    }


    /**
     * 根据文件名生成存储路径
     */
    public String getPath(String fileName) {
        return getPath() + "/" + getNewFileName(fileName);
    }

    /**
     * 文件上传
     * @return http 地址
     */
    public abstract String upload(byte[] data, String path);

    /**
     * 文件上传
     * @return 返回http地址
     */
    public abstract String upload(InputStream inputStream, String path);


}
