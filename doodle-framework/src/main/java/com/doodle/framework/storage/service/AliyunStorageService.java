package com.doodle.framework.storage.service;



import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.doodle.framework.storage.properties.StorageProperties;
import com.doodle.framework.common.exception.CustomException;
import com.doodle.framework.common.exception.ErrorEnum;
import lombok.extern.slf4j.Slf4j;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

/**
 * 阿里云存储
 */
@Slf4j
public class AliyunStorageService extends StorageService {
    
    public AliyunStorageService(StorageProperties properties) {
        this.properties = properties;
    }

    @Override
    public String upload(byte[] data, String path) {
        return upload(new ByteArrayInputStream(data), path);
    }

    @Override
    public String upload(InputStream inputStream, String path) {
        OSS client = new OSSClientBuilder().build(properties.getAliyun().getEndPoint(),
                properties.getAliyun().getAccessKeyId(), properties.getAliyun().getAccessKeySecret());
        try {
            client.putObject(properties.getAliyun().getBucketName(), path, inputStream);
        } catch (Exception e) {
            log.error("文件上传失败", e);
            throw CustomException.of(ErrorEnum.UPLOAD_FILE_ERROR);
        } finally {
            if (client != null) {
                client.shutdown();
            }
        }

        return properties.getConfig().getDomain() + "/" + path;
    }

}
