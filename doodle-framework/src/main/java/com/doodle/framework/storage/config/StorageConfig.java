package com.doodle.framework.storage.config;

import com.doodle.framework.storage.service.AliyunStorageService;
import com.doodle.framework.storage.service.LocalStorageService;
import com.doodle.framework.storage.service.StorageService;
import com.doodle.framework.storage.properties.StorageProperties;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 存储配置文件
 */
@Configuration
@EnableConfigurationProperties(StorageProperties.class)
@ConditionalOnProperty(prefix = "storage", value = "enabled")
public class StorageConfig {

    @Bean
    public StorageService storageService(StorageProperties properties) {

        return switch (properties.getConfig().getType()) {
            case LOCAL -> new LocalStorageService(properties);
            case ALIYUN -> new AliyunStorageService(properties);
        };

    }

}
