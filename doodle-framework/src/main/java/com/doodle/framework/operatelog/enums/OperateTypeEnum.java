package com.doodle.framework.operatelog.enums;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum OperateTypeEnum {
    /**
     * 查询
     */
    GET(1, "查询"),
    /**
     * 新增
     */
    INSERT(2, "新增"),
    /**
     * 修改
     */
    UPDATE(3, "修改"),
    /**
     * 删除
     */
    DELETE(4, "删除"),
    /**
     * 导出
     */
    EXPORT(5, "导出"),
    /**
     * 导入
     */
    IMPORT(6, "导入"),
    /**
     * 退出登录
     */
    LOGOUT(7, "退出登录"),
    /**
     * 注册
     */
    SIGNUP(8, "注册"),
    /**
     * 上传（对象存储）
     */
    UPLOAD(9, "上传"),
    /**
     * 下载（对象存储）
     */
    DOWNLOAD(10, "下载"),
    /**
     * 其它
     */
    OTHER(0, "其它");

    @JsonValue
    @EnumValue
    private final int code;
    private final String label;
}
