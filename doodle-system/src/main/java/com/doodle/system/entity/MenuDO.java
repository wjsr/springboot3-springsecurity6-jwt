package com.doodle.system.entity;


import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

@Data
@TableName("t_menu")
public class MenuDO {
    @TableId(type = IdType.AUTO)
    private Long menuId;
    private Integer menuType;
    private String title;
    @TableField(insertStrategy = FieldStrategy.NOT_EMPTY)
    private String icon;
    private Long parentId;
    @TableField(insertStrategy = FieldStrategy.NOT_NULL)
    private String name;
    private String path;
    @TableField(insertStrategy = FieldStrategy.NOT_NULL)
    private String query;
    private String component;
    private Boolean isExternal;
    private Integer sort;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
