package com.doodle.system.entity;


import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

@Data
@TableName("t_dict_data")
public class DictDataDO {
    @TableId(type = IdType.AUTO)
    private Long id;
    private Long dictId;
    private String dictLabel;
    private Integer dictCode;
    private String dictValue;
    private Integer sort;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
