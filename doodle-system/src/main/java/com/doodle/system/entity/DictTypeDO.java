package com.doodle.system.entity;


import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

@Data
@TableName("t_dict_type")
public class DictTypeDO {
    @TableId(type = IdType.AUTO)
    private Long dictId;
    private String dictName;
    private String dictType;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
