package com.doodle.system.entity;


import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import java.util.Date;

@Data
@TableName("t_role")
public class RoleDO {
    @TableId(type = IdType.AUTO)
    private Integer roleId;
    private Integer roleCode;
    private String roleName;
    private Integer sort;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
