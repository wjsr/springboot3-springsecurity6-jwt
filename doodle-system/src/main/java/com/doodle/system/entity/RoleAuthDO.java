package com.doodle.system.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

@Data
@TableName("t_role_auth")
public class RoleAuthDO {
    @TableId(type = IdType.AUTO)
    private Integer roleAuthId;
    private Integer roleId;
    private Integer authId;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
}
