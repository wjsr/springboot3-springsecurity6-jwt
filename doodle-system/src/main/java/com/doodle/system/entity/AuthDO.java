package com.doodle.system.entity;


import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import java.util.Date;

@Data
@TableName("t_auth")
public class AuthDO {
    @TableId(type = IdType.AUTO)
    private Integer authId;
    private Integer authCode;
    private String authName;
    private String permission;
    private Integer categoryId;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
