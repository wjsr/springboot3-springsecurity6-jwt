package com.doodle.system.entity;


import com.baomidou.mybatisplus.annotation.*;
import com.doodle.framework.operatelog.enums.OperateTypeEnum;
import lombok.Data;

import java.util.Date;

@Data
@TableName("t_operate_log")
public class OperateLogDO {
    @TableId(type = IdType.AUTO)
    private Long operateId;
    private String module;
    private String name;
    private String reqUri;
    private String reqMethod;
    private String reqParams;
    private String ip;
    private String address;
    private String userAgent;
    private OperateTypeEnum operateType;
    private Long duration;
    private Integer status;
    private Long userId;
    private String resultMsg;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
}
