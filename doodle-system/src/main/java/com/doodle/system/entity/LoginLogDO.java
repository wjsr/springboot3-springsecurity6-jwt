package com.doodle.system.entity;


import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

import java.util.Date;

@Data
@TableName("t_login_log")
public class LoginLogDO {
    @TableId(type = IdType.AUTO)
    private Long loginId;
    private String username;
    private String ip;
    private String address;
    private String userAgent;
    private Integer status;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
}
