package com.doodle.system.entity;


import com.baomidou.mybatisplus.annotation.*;
import com.doodle.system.enums.UserStatusEnum;
import lombok.Data;

import java.util.Date;

@Data
@TableName("t_user")
public class UserDO {

    @TableId(type = IdType.AUTO)
    private Long userId;
    private String username;
    private String password;
    private String nickname;
    private String avatar;
    private String phone;
    private String email;
    private Integer sex;
    private Boolean disabled;
    @TableLogic
    private Boolean deleted;
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    public UserStatusEnum getUserStatus() {
        if (deleted == null || disabled == null) {
            return null;
        }
        if (deleted) {
            return UserStatusEnum.DELETED;
        } else if (disabled) {
            return UserStatusEnum.DISABLED;
        } else {
            return UserStatusEnum.NORMAL;
        }
    }
}
