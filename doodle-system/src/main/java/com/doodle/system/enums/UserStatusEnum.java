package com.doodle.system.enums;


import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum UserStatusEnum {
    NORMAL(1, "normal", "正常"),
    DISABLED(2, "disabled", "禁用"),
    DELETED(3, "deleted", "注销"),
    ;

    @JsonValue
    @EnumValue
    private final Integer code;
    private final String value;
    private final String label;

    @JsonCreator
    public static UserStatusEnum of(Integer code){
        for (UserStatusEnum userStatusEnum : UserStatusEnum.values()){
            if (userStatusEnum.getCode().equals(code)){
                return userStatusEnum;
            }
        }
        return null;
    }
}
