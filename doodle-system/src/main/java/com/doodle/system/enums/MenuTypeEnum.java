package com.doodle.system.enums;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MenuTypeEnum {

    CATALOG(1, "catalog", "目录"),
    MENU(2, "menu", "菜单"),
    ;

    @JsonValue
    private final Integer code;
    private final String value;
    private final String label;

    @JsonCreator
    public static MenuTypeEnum of(Integer code){
        for (MenuTypeEnum value : MenuTypeEnum.values()){
            if (value.getCode().equals(code)){
                return value;
            }
        }
        return null;
    }
}
