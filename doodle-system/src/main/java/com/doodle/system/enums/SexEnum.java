package com.doodle.system.enums;


import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SexEnum {
    MALE(1, "male", "男"),
    FEMALE(2, "female", "女"),
    UNKNOWN(3, "unknown", "未知"),
    ;

    @JsonValue
    @EnumValue
    private final Integer code;
    private final String value;
    private final String label;

    @JsonCreator
    public SexEnum of(Integer code) {
        for (SexEnum sexEnum : SexEnum.values()) {
            if (sexEnum.getCode().equals(code)) {
                return sexEnum;
            }
        }
        return null;
    }
}
