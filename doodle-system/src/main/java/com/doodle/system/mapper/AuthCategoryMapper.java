package com.doodle.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.doodle.system.entity.AuthCategoryDO;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AuthCategoryMapper extends BaseMapper<AuthCategoryDO> {
}
