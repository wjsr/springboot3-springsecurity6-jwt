package com.doodle.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.doodle.system.entity.AuthDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface AuthMapper extends BaseMapper<AuthDO> {
    List<AuthDO> getByCategoryIds(@Param("categoryIds") List<Integer> categoryIds);
}
