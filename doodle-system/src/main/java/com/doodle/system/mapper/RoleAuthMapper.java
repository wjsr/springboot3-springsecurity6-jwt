package com.doodle.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.doodle.system.entity.AuthDO;
import com.doodle.system.entity.RoleAuthDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface RoleAuthMapper extends BaseMapper<RoleAuthDO> {
    List<AuthDO> getByRoleId(@Param("roleId") Integer roleId);

    void deleteByRoleId(@Param("roleId") Integer roleId);
}
