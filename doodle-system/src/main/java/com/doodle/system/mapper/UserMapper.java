package com.doodle.system.mapper;



import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.doodle.framework.security.enums.AuthEnum;
import com.doodle.framework.security.enums.RoleEnum;
import com.doodle.system.controller.pojo.query.AdminUserQuery;
import com.doodle.system.entity.UserDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface UserMapper extends BaseMapper<UserDO> {

    List<RoleEnum> getRoles(@Param("userId") Long userId);

    List<AuthEnum> getAuths(@Param("userId") Long userId);

    IPage<UserDO> getList(IPage<UserDO> page, @Param("query") AdminUserQuery query);

    UserDO getById(@Param("userId") Long userId);
}
