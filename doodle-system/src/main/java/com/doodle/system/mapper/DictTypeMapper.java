package com.doodle.system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.doodle.system.controller.pojo.query.AdminDictQuery;
import com.doodle.system.controller.pojo.vo.AdminDictVO;
import com.doodle.system.entity.DictTypeDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface DictTypeMapper extends BaseMapper<DictTypeDO> {
    IPage<AdminDictVO> getList(IPage<DictTypeDO> page, @Param("query") AdminDictQuery query);
}
