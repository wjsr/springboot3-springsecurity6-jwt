package com.doodle.system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.doodle.system.controller.pojo.query.LoginLogQuery;
import com.doodle.system.controller.pojo.vo.LoginLogVO;
import com.doodle.system.entity.LoginLogDO;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface LoginLogMapper extends BaseMapper<LoginLogDO> {

    IPage<LoginLogVO> getList(IPage<LoginLogDO> page, @Param("query") LoginLogQuery query);

    @Delete("delete from t_login_log where 1")
    void clear();
}
