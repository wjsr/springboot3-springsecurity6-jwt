package com.doodle.system.mapper;



import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.doodle.system.controller.pojo.query.OperateLogQuery;
import com.doodle.system.controller.pojo.vo.OperateLogVO;
import com.doodle.system.entity.OperateLogDO;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface OperateLogMapper extends BaseMapper<OperateLogDO> {

    IPage<OperateLogVO> getList(IPage<OperateLogDO> page, @Param("query") OperateLogQuery query);

    // where 1 跳过 mybatis 全表操作拦截
    @Delete("delete from t_operate_log where 1")
    void clear();
}
