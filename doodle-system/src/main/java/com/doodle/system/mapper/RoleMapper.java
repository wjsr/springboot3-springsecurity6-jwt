package com.doodle.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.doodle.system.controller.pojo.query.AdminRoleQuery;
import com.doodle.system.controller.pojo.vo.AdminRoleVO;
import com.doodle.system.entity.RoleDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface RoleMapper extends BaseMapper<RoleDO> {
    IPage<AdminRoleVO> getList(@Param("page") IPage<RoleDO> page, @Param("query") AdminRoleQuery query);
}
