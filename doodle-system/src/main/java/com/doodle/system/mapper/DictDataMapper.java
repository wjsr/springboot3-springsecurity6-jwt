package com.doodle.system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.doodle.system.controller.pojo.query.AdminDictDataQuery;
import com.doodle.system.controller.pojo.vo.AdminDictDataVO;
import com.doodle.system.controller.pojo.vo.DictDataVO;
import com.doodle.system.entity.DictDataDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface DictDataMapper extends BaseMapper<DictDataDO> {

    List<DictDataVO> getByType(@Param("dictType") String dictType);

    void deleteByDictIds(@Param("dictIds") Long[] dictIds);

    IPage<AdminDictDataVO> getList(IPage<DictDataDO> page, @Param("query") AdminDictDataQuery query);
}
