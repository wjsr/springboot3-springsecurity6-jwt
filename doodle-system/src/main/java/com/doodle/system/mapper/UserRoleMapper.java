package com.doodle.system.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.doodle.system.entity.UserRoleDO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


@Mapper
public interface UserRoleMapper extends BaseMapper<UserRoleDO> {

    void deleteByUserId(@Param("userId") Long userId);

    void deleteByUserIds(Long[] userIds);
}
