package com.doodle.system.service;


import com.doodle.framework.common.model.PageResult;
import com.doodle.framework.mybatis.service.BaseService;
import com.doodle.framework.security.enums.RoleEnum;
import com.doodle.system.controller.pojo.query.AdminUserQuery;
import com.doodle.system.controller.pojo.ro.*;
import com.doodle.system.controller.pojo.vo.AdminUserVO;
import com.doodle.system.controller.pojo.vo.TokenVO;
import com.doodle.system.controller.pojo.vo.UserVO;
import com.doodle.system.entity.UserDO;

import java.util.Set;


public interface UserService extends BaseService<UserDO> {
    void signup(SignupRO ro);

    TokenVO login(LoginRO ro);

    void addUser(AdminCreateUserRO ro);

    PageResult<AdminUserVO> getList(AdminUserQuery query);

    void deleteUser(Long[] userId);

    UserVO getInfo();

    void logout();

    void updateUserAvatar(Long userId, String url);

    void updateUserProfile(Long userId, UpdateProfileRO ro);

    void updateUserPwd(Long userId, UpdatePwdRO ro);

    AdminUserVO getInfo(Long userId);

    void resetUserPwd(Long userId, String newPassword);

    void updateUserInfo(Long userId, AdminUpdateUserRO ro);

    void updateUserRole(Long userId, Set<RoleEnum> roles);

    void export(AdminUserQuery query);
}
