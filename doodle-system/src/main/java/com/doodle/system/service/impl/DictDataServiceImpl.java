package com.doodle.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.doodle.framework.common.model.PageResult;
import com.doodle.framework.common.exception.CustomException;
import com.doodle.framework.mybatis.service.impl.BaseServiceImpl;
import com.doodle.system.controller.pojo.query.AdminDictDataQuery;
import com.doodle.system.controller.pojo.ro.AdminDictDataRO;
import com.doodle.system.controller.pojo.vo.AdminDictDataVO;
import com.doodle.system.controller.pojo.vo.DictDataVO;
import com.doodle.system.entity.DictDataDO;
import com.doodle.system.entity.DictTypeDO;
import com.doodle.system.mapper.DictDataMapper;
import com.doodle.system.mapper.DictTypeMapper;
import com.doodle.system.service.DictDataService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class DictDataServiceImpl extends BaseServiceImpl<DictDataMapper, DictDataDO> implements DictDataService {

    private final DictDataMapper dictDataMapper;
    private final DictTypeMapper dictTypeMapper;

    @Override
    public List<DictDataVO> getDictDataByType(String dictType) {

        return dictDataMapper.getByType(dictType);
    }

    @Override
    public PageResult<AdminDictDataVO> getList(AdminDictDataQuery query) {

        IPage<AdminDictDataVO> page = dictDataMapper.getList(getPage(query), query);

        return new PageResult<>(page.getRecords(), page.getTotal());
    }

    @Override
    public AdminDictDataVO getInfo(Long id) {
        DictDataDO dictDataDO = Optional.ofNullable(dictDataMapper.selectById(id))
                .orElseThrow(() -> CustomException.of("字典数据不存在"));

        Optional<DictTypeDO> dictTypeDO =
                Optional.ofNullable(dictTypeMapper.selectById(dictDataDO.getDictId()));


        AdminDictDataVO vo = new AdminDictDataVO();
        BeanUtils.copyProperties(dictDataDO, vo);
        dictTypeDO.ifPresent(type -> vo.setDictType(type.getDictType()));

        return vo;
    }

    @Override
    public void addData(AdminDictDataRO ro) {

        DictTypeDO dictTypeDO = Optional.ofNullable(dictTypeMapper.selectOne(
                new QueryWrapper<DictTypeDO>().eq("dict_type", ro.getDictType())
        )).orElseThrow(() -> CustomException.of("不存在的字典类型"));

        DictDataDO dictDataDO = new DictDataDO();
        BeanUtils.copyProperties(ro, dictDataDO);
        dictDataDO.setDictId(dictTypeDO.getDictId());

        dictDataMapper.insert(dictDataDO);
    }

    @Override
    public void updateData(Long id, AdminDictDataRO ro) {
        DictDataDO dictDataDO = Optional.ofNullable(dictDataMapper.selectById(id))
                .orElseThrow(() -> CustomException.of("字典数据不存在"));

        Optional.ofNullable(dictTypeMapper.selectOne(
                new QueryWrapper<DictTypeDO>().eq("dict_type", ro.getDictType())
        )).orElseThrow(() -> CustomException.of("不存在的字典类型"));

        BeanUtils.copyProperties(ro, dictDataDO);

        dictDataMapper.updateById(dictDataDO);
    }

    @Override
    public void deleteData(Long[] ids) {

        dictDataMapper.deleteBatchIds(Arrays.asList(ids));
    }
}
