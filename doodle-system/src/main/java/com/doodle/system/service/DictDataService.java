package com.doodle.system.service;

import com.doodle.framework.common.model.PageResult;
import com.doodle.framework.mybatis.service.BaseService;
import com.doodle.system.controller.pojo.query.AdminDictDataQuery;
import com.doodle.system.controller.pojo.ro.AdminDictDataRO;
import com.doodle.system.controller.pojo.vo.AdminDictDataVO;
import com.doodle.system.controller.pojo.vo.DictDataVO;
import com.doodle.system.entity.DictDataDO;

import java.util.List;

public interface DictDataService extends BaseService<DictDataDO> {
    List<DictDataVO> getDictDataByType(String dictType);

    PageResult<AdminDictDataVO> getList(AdminDictDataQuery query);

    AdminDictDataVO getInfo(Long id);

    void addData(AdminDictDataRO ro);

    void updateData(Long id, AdminDictDataRO ro);

    void deleteData(Long[] ids);
}
