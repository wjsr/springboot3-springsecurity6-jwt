package com.doodle.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fhs.trans.service.impl.DictionaryTransService;
import com.doodle.framework.common.model.PageResult;
import com.doodle.framework.common.exception.CustomException;
import com.doodle.framework.mybatis.service.impl.BaseServiceImpl;
import com.doodle.system.controller.pojo.query.AdminDictQuery;
import com.doodle.system.controller.pojo.ro.AdminDictRO;
import com.doodle.system.controller.pojo.vo.AdminDictVO;
import com.doodle.system.entity.DictDataDO;
import com.doodle.system.entity.DictTypeDO;
import com.doodle.system.mapper.DictDataMapper;
import com.doodle.system.mapper.DictTypeMapper;
import com.doodle.system.service.DictTypeService;
import lombok.AllArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class DictTypeServiceImpl extends BaseServiceImpl<DictTypeMapper, DictTypeDO> implements DictTypeService, InitializingBean {

    private final DictTypeMapper dictTypeMapper;
    private final DictDataMapper dictDataMapper;
    private final DictionaryTransService dictionaryTransService;


    // 属性注入完成后执行
    @Override
    public void afterPropertiesSet() {
        refreshTransCache();
    }

    @Override
    public void refreshTransCache() {
        // 异步缓存字典，减少启动用时
        CompletableFuture.supplyAsync(() -> {
            List<DictDataDO> dictDataList = dictDataMapper.selectList(null);
            Map<Long, List<DictDataDO>> dictTypeDataMap = dictDataList.stream()
                    .collect(Collectors.groupingBy(DictDataDO::getDictId));

            List<DictTypeDO> dictTypeList = super.list();
            for (DictTypeDO dictTypeDO : dictTypeList) {
                if (dictTypeDataMap.containsKey(dictTypeDO.getDictId())) {
                    dictionaryTransService.refreshCache(dictTypeDO.getDictType(),
                            dictTypeDataMap.get(dictTypeDO.getDictId())
                                    .stream().collect(Collectors
                                            .toMap(data -> data.getDictCode().toString(),
                                                    DictDataDO::getDictLabel)));
                }
            }
            return null;
        });
    }

    @Override
    public PageResult<AdminDictVO> getList(AdminDictQuery query) {

        IPage<AdminDictVO> page = dictTypeMapper.getList(getPage(query), query);

        return new PageResult<>(page.getRecords(), page.getTotal());
    }

    @Override
    public AdminDictVO getInfo(Long dictId) {
        DictTypeDO dictTypeDO = Optional.ofNullable(dictTypeMapper.selectById(dictId))
                .orElseThrow(() -> CustomException.of("字典不存在"));

        AdminDictVO vo = new AdminDictVO();
        BeanUtils.copyProperties(dictTypeDO, vo);

        return vo;
    }

    @Override
    public void updateDict(Long dictId, AdminDictRO ro) {
        DictTypeDO dictTypeDO = Optional.ofNullable(dictTypeMapper.selectById(dictId))
                .orElseThrow(() -> CustomException.of("字典不存在"));

        dictTypeDO.setDictName(ro.getDictName());
        dictTypeDO.setDictType(ro.getDictType());

        dictTypeMapper.updateById(dictTypeDO);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteDict(Long[] dictIds) {
        // 删除关联的字典数据
        dictDataMapper.deleteByDictIds(dictIds);

        // 删除自身
        dictTypeMapper.deleteBatchIds(Arrays.asList(dictIds));
    }

    @Override
    public void addDict(AdminDictRO ro) {
        QueryWrapper<DictTypeDO> wrapper = new QueryWrapper<DictTypeDO>()
                .eq("dict_name", ro.getDictName());
        if (dictTypeMapper.exists(wrapper)) {
            throw CustomException.of("字典名称已经存在");
        }
        wrapper.clear();
        wrapper.eq("dict_type", ro.getDictType());
        if (dictTypeMapper.exists(wrapper)) {
            throw CustomException.of("字典类型已经存在");
        }

        DictTypeDO dictTypeDO = new DictTypeDO();
        dictTypeDO.setDictName(ro.getDictName());
        dictTypeDO.setDictType(ro.getDictType());

        dictTypeMapper.insert(dictTypeDO);
    }
}
