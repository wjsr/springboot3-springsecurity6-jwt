package com.doodle.system.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fhs.trans.service.impl.TransService;
import com.doodle.framework.cache.RedisCache;
import com.doodle.framework.cache.RedisKeys;
import com.doodle.framework.common.constant.Constants;
import com.doodle.framework.common.model.PageResult;
import com.doodle.framework.common.utils.DateUtils;
import com.doodle.framework.common.utils.ExcelUtils;
import com.doodle.framework.common.utils.FakerUtils;
import com.doodle.framework.common.utils.JwtUtils;
import com.doodle.framework.common.exception.CustomException;
import com.doodle.framework.mybatis.service.impl.BaseServiceImpl;
import com.doodle.framework.security.enums.AuthEnum;
import com.doodle.framework.security.enums.RoleEnum;
import com.doodle.framework.security.utils.SecurityUtils;
import com.doodle.system.controller.pojo.export.UserExport;
import com.doodle.system.controller.pojo.query.AdminUserQuery;
import com.doodle.system.controller.pojo.ro.*;
import com.doodle.system.controller.pojo.vo.AdminUserVO;
import com.doodle.system.entity.RoleDO;
import com.doodle.system.enums.SexEnum;
import com.doodle.system.enums.UserStatusEnum;
import com.doodle.system.mapper.UserRoleMapper;
import com.doodle.system.controller.pojo.vo.TokenVO;
import com.doodle.system.controller.pojo.vo.UserVO;
import com.doodle.system.entity.UserDO;
import com.doodle.system.entity.UserRoleDO;
import com.doodle.system.mapper.UserMapper;
import com.doodle.system.overide.MyUserDetails;
import com.doodle.system.service.RoleService;
import com.doodle.system.service.UserRoleService;
import com.doodle.system.service.UserService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


@Service
@RequiredArgsConstructor
public class UserServiceImpl extends BaseServiceImpl<UserMapper, UserDO> implements UserService {

    @Value("${account.init-password}")
    private String initPassword;

    private final JwtUtils jwtUtils;
    private final RedisCache redisCache;
    private final PasswordEncoder passwordEncoder;
    private final UserMapper userMapper;
    private final UserRoleService userRoleService;
    private final UserRoleMapper userRoleMapper;
    private final AuthenticationManager authenticationManager;
    private final RoleService roleService;
    private final TransService transService;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void signup(SignupRO ro) {
        // 验证码校验
        validateCaptcha(ro.getCode(), ro.getUuid());

        if (isUsernameExist(ro.getUsername())) {
            throw CustomException.of("用户已经存在");
        }

        // 创建用户
        UserDO user = new UserDO();
        user.setUsername(ro.getUsername());
        user.setPassword(passwordEncoder.encode(ro.getPassword()));
        user.setSex(SexEnum.UNKNOWN.getCode());
        user.setNickname(FakerUtils.generateRandomNickname(8));
        user.setDisabled(false);
        user.setDeleted(false);
        userMapper.insert(user);

        // 关联默认角色
        relDefaultRole(user.getUserId());
    }

    /**
     * 关联默认角色
     */
    @Transactional(rollbackFor = Exception.class)
    public void relDefaultRole(Long userId) {
        RoleDO role = roleService.getByCode(RoleEnum.USER);
        UserRoleDO userRoleDO = new UserRoleDO();
        userRoleDO.setUserId(userId);
        userRoleDO.setRoleId(role.getRoleId());
        userRoleMapper.insert(userRoleDO);
    }

    // 判断用户名是否已经存在
    private boolean isUsernameExist(String username) {
        UserDO user = userMapper.selectOne(new QueryWrapper<UserDO>().eq("username", username));
        return user != null;
    }

    @Override
    public TokenVO login(LoginRO ro) {
        // 验证码校验
        validateCaptcha(ro.getCode(), ro.getUuid());

        // 对账号密码进行校验，认证失败会自己抛异常
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(ro.getUsername(), ro.getPassword())
        );

        // 生成 token
        MyUserDetails userDetails = (MyUserDetails) authentication.getPrincipal();
        String token = jwtUtils.generateToken(userDetails.getUsername(), userDetails.getUserId());
        return TokenVO.builder().token(token).build();
    }

    /**
     * 验证码校验
     */
    public void validateCaptcha(String code, String uuid) {
        String key = RedisKeys.getCaptchaCodeKey(uuid);
        String captcha = (String) redisCache.get(key);
        redisCache.delete(key);
        if (captcha == null) {
            throw CustomException.of("验证码已失效");
        }
        if (!code.equalsIgnoreCase(captcha)) {
            throw CustomException.of("验证码错误");
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void addUser(AdminCreateUserRO ro) {

        // 判断用户是否已经存在
        if (isUsernameExist(ro.getUsername())) {
            throw CustomException.of("用户已经存在");
        }

        // 创建用户
        UserDO user = new UserDO();
        user.setUsername(ro.getUsername());
        user.setPassword(passwordEncoder.encode(initPassword));
        user.setNickname(ro.getNickname());
        user.setPhone(ro.getPhone());
        user.setEmail(ro.getEmail());
        user.setSex(ro.getSex().getCode());
        user.setDisabled(ro.getStatus() == UserStatusEnum.DISABLED);
        user.setDeleted(false);
        userMapper.insert(user);

        // 关联默认角色
        relDefaultRole(user.getUserId());
    }


    @Override
    public PageResult<AdminUserVO> getList(AdminUserQuery query) {
        query.setEndTime(DateUtils.endOfDay(query.getEndTime()));

        IPage<UserDO> page = userMapper.getList(getPage(query), query);

        List<AdminUserVO> list = page.getRecords().stream().map(user -> {
            AdminUserVO vo = new AdminUserVO();
            BeanUtils.copyProperties(user, vo);
            vo.setStatus(user.getUserStatus());
            vo.setRoles(userMapper.getRoles(user.getUserId()));
            return vo;
        }).toList();

        return new PageResult<>(list, page.getTotal());
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deleteUser(Long[] userIds) {

        // 删除关联的角色
        userRoleMapper.deleteByUserIds(userIds);

        // 删除用户
        userMapper.deleteBatchIds(Arrays.asList(userIds));
    }

    @Override
    public UserVO getInfo() {

        UserDO user = Optional.ofNullable(userMapper.selectById(SecurityUtils.getUserId()))
                .orElseThrow(() -> CustomException.of("用户不存在"));

        UserVO vo = new UserVO();
        BeanUtils.copyProperties(user, vo);
        vo.setRoles(SecurityUtils.getRoles().stream().map(RoleEnum::getValue).toList());
        vo.setAuths(SecurityUtils.getAuths().stream().map(AuthEnum::getValue).toList());

        return vo;
    }

    /**
     * 退出登录
     */
    @Override
    public void logout() {
        jwtUtils.delCacheToken(SecurityUtils.getUserId());
    }

    @Override
    public void updateUserAvatar(Long userId, String url) {
        Optional<UserDO> optional = Optional.ofNullable(userMapper.selectById(userId));
        UserDO userDO = optional.orElseThrow(() -> CustomException.of("用户不存在"));
        userDO.setAvatar(url);

        userMapper.updateById(userDO);
    }

    @Override
    public void updateUserProfile(Long userId, UpdateProfileRO ro) {
        UserDO userDO = Optional.ofNullable(userMapper.selectById(userId))
                .orElseThrow(() -> CustomException.of("用户不存在"));

        BeanUtils.copyProperties(ro, userDO);
        userDO.setSex(ro.getSex().getCode());

        userMapper.updateById(userDO);
    }

    @Override
    public void updateUserPwd(Long userId, UpdatePwdRO ro) {
        UserDO userDO = Optional.ofNullable(userMapper.selectById(userId))
                .orElseThrow(() -> CustomException.of("用户不存在"));

        if (!passwordEncoder.matches(ro.getOldPassword(), userDO.getPassword())) {
            throw CustomException.of("旧密码不正确");
        }

        if (passwordEncoder.matches(ro.getNewPassword(), userDO.getPassword())) {
            throw CustomException.of("新密码不能与旧密码相同");
        }

        userDO.setPassword(passwordEncoder.encode(ro.getNewPassword()));

        userMapper.updateById(userDO);
    }

    @Override
    public AdminUserVO getInfo(Long userId) {
        UserDO userDO = userMapper.getById(userId);
        if (userDO == null) {
            throw CustomException.of("用户不存在");
        }
        AdminUserVO vo = new AdminUserVO();
        BeanUtils.copyProperties(userDO, vo);
        vo.setStatus(userDO.getUserStatus());
        vo.setRoles(userMapper.getRoles(userId));

        return vo;
    }

    @Override
    public void resetUserPwd(Long userId, String newPassword) {
        UserDO userDO = Optional.ofNullable(userMapper.selectById(userId))
                .orElseThrow(() -> CustomException.of("用户不存在"));

        if (passwordEncoder.matches(newPassword, userDO.getPassword())) {
            throw CustomException.of("新密码不能与旧密码相同");
        }

        userDO.setPassword(passwordEncoder.encode(newPassword));

        userMapper.updateById(userDO);
    }

    @Override
    public void updateUserInfo(Long userId, AdminUpdateUserRO ro) {

        UserDO userDO = Optional.ofNullable(userMapper.selectById(userId))
                .orElseThrow(() -> CustomException.of("用户不存在"));

        BeanUtils.copyProperties(ro, userDO);
        userDO.setSex(ro.getSex().getCode());
        switch (ro.getStatus()) {
            case NORMAL -> userDO.setDisabled(false);
            case DISABLED -> userDO.setDisabled(true);
        }

        userMapper.updateById(userDO);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateUserRole(Long userId, Set<RoleEnum> roles) {
        if (CollectionUtils.isEmpty(roles)) {
            throw CustomException.of("没有传递需关联的角色");
        }
        List<RoleDO> roleList = roleService.getByCodes(roles);
        if (roleList.size() != roles.size()) {
            throw CustomException.of("存在非法的角色");
        }

        Optional.ofNullable(userMapper.selectById(userId))
                .orElseThrow(() -> CustomException.of("用户不存在"));

        // 删除原有角色
        userRoleMapper.deleteByUserId(userId);

        // 添加角色
        List<UserRoleDO> userRoleList = roleList.stream().map(roleDO -> {
            UserRoleDO userRoleDO = new UserRoleDO();
            userRoleDO.setUserId(userId);
            userRoleDO.setRoleId(roleDO.getRoleId());
            return userRoleDO;
        }).toList();
        userRoleService.saveBatch(userRoleList);
    }

    @Override
    public void export(AdminUserQuery query) {
        query.setEndTime(DateUtils.endOfDay(query.getEndTime()));
        List<AdminUserVO> rows = this.getList(query).getRows();
        List<UserExport> exports = rows.stream().map(row -> {
            UserExport export = new UserExport();
            BeanUtils.copyProperties(row, export);
            export.setId(row.getUserId());
            List<String> roleList = row.getRoles().stream().filter(Objects::nonNull).map(RoleEnum::getLabel).toList();
            export.setRoleLabel(StringUtils.join(roleList, ","));
            return export;
        }).toList();
        transService.transBatch(exports);
        ExcelUtils.excelExport(UserExport.class, "user_excel" + DateUtils.format(new Date(), Constants.DATE_PATTERN), null, exports);
    }
}
