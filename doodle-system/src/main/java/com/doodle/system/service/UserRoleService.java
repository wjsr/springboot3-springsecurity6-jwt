package com.doodle.system.service;

import com.doodle.framework.mybatis.service.BaseService;
import com.doodle.system.entity.UserRoleDO;

public interface UserRoleService extends BaseService<UserRoleDO> {
}
