package com.doodle.system.service.impl;


import com.doodle.framework.mybatis.service.impl.BaseServiceImpl;
import com.doodle.system.entity.UserRoleDO;
import com.doodle.system.mapper.UserRoleMapper;
import com.doodle.system.service.UserRoleService;
import org.springframework.stereotype.Service;

@Service
public class UserRoleServiceImpl extends BaseServiceImpl<UserRoleMapper, UserRoleDO> implements UserRoleService {
}
