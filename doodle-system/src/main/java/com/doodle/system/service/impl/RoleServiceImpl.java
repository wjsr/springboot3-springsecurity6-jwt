package com.doodle.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.doodle.framework.common.model.PageResult;
import com.doodle.framework.common.exception.CustomException;
import com.doodle.framework.mybatis.service.impl.BaseServiceImpl;
import com.doodle.framework.security.enums.RoleEnum;
import com.doodle.system.controller.pojo.query.AdminRoleQuery;
import com.doodle.system.controller.pojo.ro.AdminRoleRO;
import com.doodle.system.controller.pojo.vo.AdminRoleVO;
import com.doodle.system.controller.pojo.vo.RoleInfoVO;
import com.doodle.system.entity.AuthDO;
import com.doodle.system.entity.RoleAuthDO;
import com.doodle.system.entity.RoleDO;
import com.doodle.system.mapper.AuthMapper;
import com.doodle.system.mapper.RoleAuthMapper;
import com.doodle.system.mapper.RoleMapper;
import com.doodle.system.service.RoleAuthService;
import com.doodle.system.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl extends BaseServiceImpl<RoleMapper, RoleDO> implements RoleService {

    private final RoleMapper roleMapper;
    private final RoleAuthService roleAuthService;
    private final RoleAuthMapper roleAuthMapper;
    private final AuthMapper authMapper;

    @Override
    public PageResult<AdminRoleVO> getList(AdminRoleQuery query) {

        IPage<AdminRoleVO> page = roleMapper.getList(getPage(query), query);

        return new PageResult<>(page.getRecords(), page.getSize());
    }

    @Override
    public RoleInfoVO getInfo(Integer roleId) {
        RoleDO roleDO = Optional.ofNullable(roleMapper.selectById(roleId))
                .orElseThrow(() -> CustomException.of("角色不存在"));
        RoleInfoVO vo = new RoleInfoVO();
        BeanUtils.copyProperties(roleDO, vo);
        List<AuthDO> auths = roleAuthMapper.getByRoleId(roleId);
        vo.setAuthIds(auths.stream().map(AuthDO::getAuthId).toList());

        return vo;
    }

    @Override
    public RoleDO getByCode(RoleEnum role) {
        return roleMapper.selectOne(
                new QueryWrapper<RoleDO>().eq("role_code", role.getCode())
        );
    }

    @Override
    public List<RoleDO> getByCodes(Collection<RoleEnum> roles) {
        return roleMapper.selectList(
                new QueryWrapper<RoleDO>().in("role_code",
                        roles.stream().map(RoleEnum::getCode).toList())
        );
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updateRole(Integer roleId, AdminRoleRO ro) {
        RoleDO roleDO = Optional.ofNullable(roleMapper.selectById(roleId))
                .orElseThrow(() -> CustomException.of("角色不存在"));
        if (RoleEnum.of(roleDO.getRoleCode()) == RoleEnum.SUPER_ADMIN) {
            throw CustomException.of("不能对超级管理员进行修改");
        }

        roleDO.setSort(ro.getSort());
        roleMapper.updateById(roleDO);

        // 删除关联的权限
        roleAuthMapper.deleteByRoleId(roleId);

        // 添加关联的权限
        if (CollectionUtils.isNotEmpty(ro.getAuthIds())) {
            List<AuthDO> authList = authMapper.selectBatchIds(ro.getAuthIds());

            List<RoleAuthDO> roleAuthList = authList.stream().map(auth -> {
                RoleAuthDO roleAuthDO = new RoleAuthDO();
                roleAuthDO.setRoleId(roleId);
                roleAuthDO.setAuthId(auth.getAuthId());
                return roleAuthDO;
            }).toList();
            roleAuthService.saveBatch(roleAuthList);
        }

    }
}
