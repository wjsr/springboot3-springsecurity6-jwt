package com.doodle.system.service;

import com.doodle.framework.common.model.PageResult;
import com.doodle.framework.mybatis.service.BaseService;
import com.doodle.system.controller.pojo.query.OperateLogQuery;
import com.doodle.system.controller.pojo.vo.OperateLogVO;
import com.doodle.system.entity.OperateLogDO;

public interface OperateLogService extends BaseService<OperateLogDO> {
    PageResult<OperateLogVO> getList(OperateLogQuery query);

    OperateLogVO getInfo(Long operateId);

    void deleteLog(Long[] operateIds);

    void clearLog();

    void export(OperateLogQuery query);
}
