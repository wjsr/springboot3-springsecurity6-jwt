package com.doodle.system.service;

import com.doodle.framework.mybatis.service.BaseService;
import com.doodle.system.entity.RoleAuthDO;

public interface RoleAuthService extends BaseService<RoleAuthDO> {
}
