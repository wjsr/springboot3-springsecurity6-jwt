package com.doodle.system.service;

import com.doodle.framework.common.model.PageResult;
import com.doodle.framework.mybatis.service.BaseService;
import com.doodle.system.controller.pojo.query.AdminDictQuery;
import com.doodle.system.controller.pojo.ro.AdminDictRO;
import com.doodle.system.controller.pojo.vo.AdminDictVO;
import com.doodle.system.entity.DictTypeDO;

public interface DictTypeService extends BaseService<DictTypeDO> {

    void refreshTransCache();

    PageResult<AdminDictVO> getList(AdminDictQuery query);

    AdminDictVO getInfo(Long dictId);

    void updateDict(Long dictId, AdminDictRO ro);

    void deleteDict(Long[] dictId);

    void addDict(AdminDictRO ro);
}
