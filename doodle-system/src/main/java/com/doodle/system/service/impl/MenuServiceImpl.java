package com.doodle.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.doodle.framework.common.exception.CustomException;
import com.doodle.framework.mybatis.service.impl.BaseServiceImpl;
import com.doodle.system.controller.pojo.ro.AdminMenuRO;
import com.doodle.system.controller.pojo.vo.AdminMenuVO;
import com.doodle.system.controller.pojo.vo.RouteMetaVO;
import com.doodle.system.controller.pojo.vo.RouteVO;
import com.doodle.system.entity.MenuDO;
import com.doodle.system.mapper.MenuMapper;
import com.doodle.system.service.MenuService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
public class MenuServiceImpl extends BaseServiceImpl<MenuMapper, MenuDO> implements MenuService {

    private final MenuMapper menuMapper;

    @Override
    public List<RouteVO> getRoutes() {
        List<MenuDO> menus = menuMapper.selectList(null);
        Map<Long, List<MenuDO>> menuMap = menus.stream().collect(Collectors.groupingBy(MenuDO::getParentId));

        List<MenuDO> rootMenus = menus.stream().filter(menu -> menu.getParentId() == 0).toList();

        return constructRoutes(rootMenus, menuMap);
    }

    // 递归构造路由列表
    private List<RouteVO> constructRoutes(List<MenuDO> rootMenus, Map<Long, List<MenuDO>> menuMap){

        return rootMenus.stream()
                .sorted(Comparator.comparing(MenuDO::getSort))
                .map(menu -> {
                    RouteVO routeVO = new RouteVO();
                    RouteMetaVO metaVO = new RouteMetaVO();
                    BeanUtils.copyProperties(menu, routeVO);
                    BeanUtils.copyProperties(menu, metaVO);
                    routeVO.setMeta(metaVO);
                    if (menuMap.containsKey(menu.getMenuId())) {
                        List<MenuDO> childMenus = menuMap.get(menu.getMenuId());
                        routeVO.setChildren(constructRoutes(childMenus, menuMap));
                    }
                    return routeVO;
                }).toList();
    }

    @Override
    public List<AdminMenuVO> getList(String title) {

        QueryWrapper<MenuDO> wrapper = new QueryWrapper<MenuDO>().like("title", title);
        List<MenuDO> menuList = menuMapper.selectList(StringUtils.isBlank(title) ? null : wrapper);

        // 找出最上层菜单
        Set<Long> menuIds = menuList.stream().map(MenuDO::getMenuId).collect(Collectors.toSet());
        List<MenuDO> rootMenus = menuList.stream()
                .filter(menu -> !menuIds.contains(menu.getParentId()))
                .toList();

        // 构造子菜单
        Map<Long, List<MenuDO>> menuMap = menuList.stream().collect(Collectors.groupingBy(MenuDO::getParentId));

        return constructMenus(rootMenus, menuMap);
    }

    private List<AdminMenuVO> constructMenus(List<MenuDO> rootMenus, Map<Long, List<MenuDO>> menuMap) {

        return rootMenus.stream()
                .sorted(Comparator.comparing(MenuDO::getSort))
                .map(menu -> {
                    AdminMenuVO parent = new AdminMenuVO();
                    BeanUtils.copyProperties(menu, parent);
                    if (menuMap.containsKey(menu.getMenuId())) {
                        List<MenuDO> children = menuMap.get(menu.getMenuId());
                        parent.setChildren(constructMenus(children, menuMap));
                    }
                    return parent;
                }).toList();
    }

    @Override
    public AdminMenuVO getInfo(Long menuId) {
        MenuDO menuDO = Optional.ofNullable(menuMapper.selectById(menuId))
                .orElseThrow(() -> CustomException.of("菜单不存在"));
        AdminMenuVO vo = new AdminMenuVO();
        BeanUtils.copyProperties(menuDO, vo);

        return vo;
    }

    @Override
    public void addMenu(AdminMenuRO ro) {
        MenuDO parentMenu = menuMapper.selectById(ro.getParentId());
        if (ro.getParentId() != 0 && parentMenu == null) {
            throw CustomException.of("父菜单不存在");
        }
        MenuDO menuDO = new MenuDO();
        BeanUtils.copyProperties(ro, menuDO);
        menuDO.setMenuType(ro.getMenuType().getCode());

        menuMapper.insert(menuDO);
    }

    @Override
    public void updateMenu(Long menuId, AdminMenuRO ro) {
        MenuDO menuDO = Optional.ofNullable(menuMapper.selectById(menuId))
                .orElseThrow(() -> CustomException.of("菜单不存在"));
        MenuDO parentMenu = menuMapper.selectById(ro.getParentId());
        if (ro.getParentId() != 0 && parentMenu == null) {
            throw CustomException.of("父菜单不存在");
        }
        BeanUtils.copyProperties(ro, menuDO);
        menuDO.setMenuType(ro.getMenuType().getCode());

        menuMapper.updateById(menuDO);
    }

    @Override
    public void delMenu(Long menuId) {
        Optional.ofNullable(menuMapper.selectById(menuId))
                .orElseThrow(() -> CustomException.of("菜单不存在"));

        Long count = menuMapper.selectCount(
                new QueryWrapper<MenuDO>().eq("parent_id", menuId)
        );
        if (count > 0) {
            throw CustomException.of("存在子菜单，禁止删除");
        }

        menuMapper.deleteById(menuId);
    }
}
