package com.doodle.system.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fhs.trans.service.impl.TransService;
import com.doodle.framework.common.constant.Constants;
import com.doodle.framework.common.model.PageResult;
import com.doodle.framework.common.utils.DateUtils;
import com.doodle.framework.common.utils.ExcelUtils;
import com.doodle.framework.common.utils.HttpContextUtils;
import com.doodle.framework.common.utils.IpUtils;
import com.doodle.framework.mybatis.service.impl.BaseServiceImpl;
import com.doodle.system.controller.pojo.export.LoginLogExport;
import com.doodle.system.controller.pojo.query.LoginLogQuery;
import com.doodle.system.controller.pojo.vo.LoginLogVO;
import com.doodle.system.entity.LoginLogDO;
import com.doodle.system.mapper.LoginLogMapper;
import com.doodle.system.service.LoginLogService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class LoginLogServiceImpl extends BaseServiceImpl<LoginLogMapper, LoginLogDO> implements LoginLogService {

    private final LoginLogMapper loginLogMapper;
    private final TransService transService;

    @Override
    public void save(String username, Integer status) {

        LoginLogDO logDO = new LoginLogDO();
        logDO.setUsername(username);
        logDO.setStatus(status);

        HttpServletRequest request = HttpContextUtils.getHttpServletRequest();
        if (request != null) {
            String userAgent = request.getHeader(HttpHeaders.USER_AGENT);
            String ip = IpUtils.getIpAddr(request);
            String address = IpUtils.getAddressByIP(ip);

            logDO.setIp(ip);
            logDO.setAddress(address);
            logDO.setUserAgent(userAgent);
        } else {
            log.info(String.format("未能获取到用户[%s]请求体", username));
        }

        loginLogMapper.insert(logDO);
    }

    @Override
    public PageResult<LoginLogVO> getList(LoginLogQuery query) {
        query.setEndTime(DateUtils.endOfDay(query.getEndTime()));

        IPage<LoginLogVO> page = loginLogMapper.getList(getPage(query), query);

        return new PageResult<>(page.getRecords(), page.getTotal());
    }


    @Override
    public void deleteLog(Long[] loginIds) {

        loginLogMapper.deleteBatchIds(Arrays.asList(loginIds));
    }

    @Override
    public void clearLog() {

        loginLogMapper.clear();
    }

    @Override
    public void export(LoginLogQuery query) {
        query.setEndTime(DateUtils.endOfDay(query.getEndTime()));
        List<LoginLogVO> rows = this.getList(query).getRows();
        List<LoginLogExport> exports = rows.stream().map(row -> {
            LoginLogExport export = new LoginLogExport();
            BeanUtils.copyProperties(row, export);
            export.setId(row.getLoginId());
            return export;
        }).toList();
        transService.transBatch(exports);
        ExcelUtils.excelExport(LoginLogExport.class, "login_log_excel" + DateUtils.format(new Date(), Constants.DATE_PATTERN), null, exports);
    }
}
