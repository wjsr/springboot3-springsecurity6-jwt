package com.doodle.system.service.impl;

import com.doodle.framework.mybatis.service.impl.BaseServiceImpl;
import com.doodle.system.entity.RoleAuthDO;
import com.doodle.system.mapper.RoleAuthMapper;
import com.doodle.system.service.RoleAuthService;
import org.springframework.stereotype.Service;

@Service
public class RoleAuthServiceImpl extends BaseServiceImpl<RoleAuthMapper, RoleAuthDO> implements RoleAuthService {
}
