package com.doodle.system.service;

import com.doodle.framework.mybatis.service.BaseService;
import com.doodle.system.controller.pojo.vo.AdminAuthTreeVO;
import com.doodle.system.entity.AuthDO;

import java.util.List;

public interface AuthService extends BaseService<AuthDO> {
    List<AdminAuthTreeVO> getTree();

}
