package com.doodle.system.service;

import com.doodle.framework.mybatis.service.BaseService;
import com.doodle.system.controller.pojo.ro.AdminMenuRO;
import com.doodle.system.controller.pojo.vo.AdminMenuVO;
import com.doodle.system.controller.pojo.vo.RouteVO;
import com.doodle.system.entity.MenuDO;

import java.util.List;

public interface MenuService extends BaseService<MenuDO> {
    List<RouteVO> getRoutes();

    List<AdminMenuVO> getList(String title);

    void addMenu(AdminMenuRO ro);

    AdminMenuVO getInfo(Long menuId);

    void updateMenu(Long menuId, AdminMenuRO ro);

    void delMenu(Long menuId);
}
