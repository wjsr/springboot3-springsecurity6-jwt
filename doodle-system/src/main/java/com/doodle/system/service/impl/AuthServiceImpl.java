package com.doodle.system.service.impl;

import com.google.common.collect.Lists;
import com.doodle.framework.mybatis.service.impl.BaseServiceImpl;
import com.doodle.system.controller.pojo.vo.AdminAuthTreeVO;
import com.doodle.system.entity.AuthCategoryDO;
import com.doodle.system.entity.AuthDO;
import com.doodle.system.mapper.AuthCategoryMapper;
import com.doodle.system.mapper.AuthMapper;
import com.doodle.system.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl extends BaseServiceImpl<AuthMapper, AuthDO> implements AuthService {

    private final AuthMapper authMapper;
    private final AuthCategoryMapper authCategoryMapper;

    @Override
    public List<AdminAuthTreeVO> getTree() {
        // 权限分类
        List<AuthCategoryDO> categoryList = authCategoryMapper.selectList(null);
        List<Integer> categoryIds = categoryList.stream().map(AuthCategoryDO::getCategoryId).toList();
        // 权限
        List<AuthDO> authList = authMapper.getByCategoryIds(categoryIds);
        Map<Integer, List<AuthDO>> categoryAuthMap =
                authList.stream().collect(Collectors.groupingBy(AuthDO::getCategoryId));

        return categoryList.stream().map(category -> {
            AdminAuthTreeVO parentVO = new AdminAuthTreeVO();
            parentVO.setId(category.getCategoryId());
            parentVO.setName(category.getName());
            parentVO.setLevel(1);
            if (categoryAuthMap.containsKey(category.getCategoryId())) {
                List<AdminAuthTreeVO> children = categoryAuthMap.get(category.getCategoryId())
                        .stream().map(auth -> {
                            AdminAuthTreeVO childVO = new AdminAuthTreeVO();
                            childVO.setId(auth.getAuthId());
                            childVO.setName(auth.getAuthName());
                            childVO.setLevel(2);
                            return childVO;
                        }).toList();
                parentVO.setChildren(children);
            } else {
                parentVO.setChildren(Lists.newArrayList());
            }
            return parentVO;
        }).toList();
    }
}
