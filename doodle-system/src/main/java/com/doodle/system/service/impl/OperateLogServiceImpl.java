package com.doodle.system.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.fhs.trans.service.impl.TransService;
import com.doodle.framework.common.constant.Constants;
import com.doodle.framework.common.model.PageResult;
import com.doodle.framework.common.utils.DateUtils;
import com.doodle.framework.common.utils.ExcelUtils;
import com.doodle.framework.common.exception.CustomException;
import com.doodle.framework.mybatis.service.impl.BaseServiceImpl;
import com.doodle.system.controller.pojo.export.OperateLogExport;
import com.doodle.system.controller.pojo.query.OperateLogQuery;
import com.doodle.system.controller.pojo.vo.OperateLogVO;
import com.doodle.system.entity.OperateLogDO;
import com.doodle.system.mapper.OperateLogMapper;
import com.doodle.system.mapper.UserMapper;
import com.doodle.system.service.OperateLogService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
@RequiredArgsConstructor
public class OperateLogServiceImpl extends BaseServiceImpl<OperateLogMapper, OperateLogDO> implements OperateLogService {

    private final OperateLogMapper operateLogMapper;
    private final UserMapper userMapper;
    private final TransService transService;

    @Override
    public PageResult<OperateLogVO> getList(OperateLogQuery query) {

        query.setEndTime(DateUtils.endOfDay(query.getEndTime()));

        IPage<OperateLogVO> page = operateLogMapper.getList(getPage(query), query);

        return new PageResult<>(page.getRecords(), page.getTotal());
    }

    @Override
    public OperateLogVO getInfo(Long operateId) {
        OperateLogDO operateLogDO = Optional.ofNullable(operateLogMapper.selectById(operateId))
                .orElseThrow(() -> CustomException.of("操作日志不存在"));

        OperateLogVO vo = new OperateLogVO();
        BeanUtils.copyProperties(operateLogDO, vo);

        Optional.ofNullable(userMapper.selectById(operateLogDO.getUserId()))
                .ifPresent(user -> vo.setUsername(user.getUsername()));

        return vo;
    }

    @Override
    public void deleteLog(Long[] operateIds) {

        operateLogMapper.deleteBatchIds(Arrays.asList(operateIds));
    }

    @Override
    public void clearLog() {

        operateLogMapper.clear();
    }

    @Override
    public void export(OperateLogQuery query) {
        query.setEndTime(DateUtils.endOfDay(query.getEndTime()));
        query.setEndTime(DateUtils.endOfDay(query.getEndTime()));
        List<OperateLogVO> rows = this.getList(query).getRows();
        List<OperateLogExport> exports = rows.stream().map(row -> {
            OperateLogExport export = new OperateLogExport();
            BeanUtils.copyProperties(row, export);
            export.setId(row.getOperateId());
            return export;
        }).toList();
        transService.transBatch(exports);
        ExcelUtils.excelExport(OperateLogExport.class, "operate_log_excel" + DateUtils.format(new Date(), Constants.DATE_PATTERN), null, exports);
    }
}
