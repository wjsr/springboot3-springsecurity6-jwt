package com.doodle.system.service;

import com.doodle.framework.common.model.PageResult;
import com.doodle.framework.mybatis.service.BaseService;
import com.doodle.framework.security.enums.RoleEnum;
import com.doodle.system.controller.pojo.query.AdminRoleQuery;
import com.doodle.system.controller.pojo.ro.AdminRoleRO;
import com.doodle.system.controller.pojo.vo.AdminRoleVO;
import com.doodle.system.controller.pojo.vo.RoleInfoVO;
import com.doodle.system.entity.RoleDO;

import java.util.Collection;
import java.util.List;

public interface RoleService extends BaseService<RoleDO> {
    PageResult<AdminRoleVO> getList(AdminRoleQuery query);

    RoleInfoVO getInfo(Integer roleId);

    RoleDO getByCode(RoleEnum role);

    List<RoleDO> getByCodes(Collection<RoleEnum> roles);

    void updateRole(Integer roleId, AdminRoleRO ro);
}
