package com.doodle.system.service;

import com.doodle.system.controller.pojo.query.LoginLogQuery;
import com.doodle.system.controller.pojo.vo.LoginLogVO;
import com.doodle.framework.common.model.PageResult;
import com.doodle.framework.mybatis.service.BaseService;
import com.doodle.system.entity.LoginLogDO;

public interface LoginLogService extends BaseService<LoginLogDO> {
    void save(String username, Integer status);

    PageResult<LoginLogVO> getList(LoginLogQuery query);

    void deleteLog(Long[] loginIds);

    void clearLog();

    void export(LoginLogQuery query);
}
