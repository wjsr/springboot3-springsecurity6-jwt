package com.doodle.system.overide;


import com.doodle.framework.security.enums.AuthEnum;
import com.doodle.framework.security.enums.RoleEnum;
import com.doodle.system.entity.UserDO;
import lombok.Builder;
import lombok.Getter;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Getter
@Builder
public class MyUserDetails implements UserDetails {

    private final UserDO user;

    private final List<RoleEnum> roles;

    private final List<AuthEnum> auths;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        // authorities 包含角色及权限
        List<GrantedAuthority> authorities = new ArrayList<>();
        if (CollectionUtils.isNotEmpty(roles)){
            roles.forEach(role -> authorities.add(new SimpleGrantedAuthority("ROLE_" + role.name())));
        }
        if (CollectionUtils.isNotEmpty(auths)){
            auths.forEach(auth -> authorities.add(new SimpleGrantedAuthority(auth.getValue())));
        }

        return authorities;
    }

    public Long getUserId() {
        return user.getUserId();
    }

    @Override
    public String getUsername() {
        return user.getUsername();
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return !user.getDisabled();
    }
}
