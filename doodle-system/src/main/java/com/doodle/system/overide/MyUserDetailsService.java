package com.doodle.system.overide;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.doodle.framework.security.enums.AuthEnum;
import com.doodle.framework.security.enums.RoleEnum;
import com.doodle.system.entity.UserDO;
import com.doodle.system.mapper.UserMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;


/**
 * 自定义获取用户详情
 */
@Component
@RequiredArgsConstructor
public class MyUserDetailsService implements UserDetailsService {

    private final UserMapper userMapper;

    @Override
    public UserDetails loadUserByUsername(String username) {

        UserDO user = userMapper.selectOne(new QueryWrapper<UserDO>().eq("username", username));

        if (user == null){
            throw new UsernameNotFoundException("用户不存在");
        }
        // 获取角色列表
        List<RoleEnum> roles = userMapper.getRoles(user.getUserId())
                .stream().filter(Objects::nonNull).toList();
        // 获取权限列表
        List<AuthEnum> auths = userMapper.getAuths(user.getUserId())
                .stream().filter(Objects::nonNull).toList();

        return MyUserDetails.builder()
                .user(user)
                .roles(roles)
                .auths(auths)
                .build();
    }
}
