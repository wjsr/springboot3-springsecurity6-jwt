package com.doodle.system.controller.pojo.query;

import com.doodle.framework.common.model.PageParam;
import lombok.Data;
import lombok.EqualsAndHashCode;


@Data
@EqualsAndHashCode(callSuper = false)
public class AdminDictQuery extends PageParam {
    private String dictName;
    private String dictType;
}
