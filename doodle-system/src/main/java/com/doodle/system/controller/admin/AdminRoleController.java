package com.doodle.system.controller.admin;


import com.doodle.framework.common.annotation.aop.NotRepeatSubmit;
import com.doodle.framework.common.model.PageResult;
import com.doodle.framework.operatelog.annotations.OperateLog;
import com.doodle.framework.operatelog.enums.OperateTypeEnum;
import com.doodle.system.controller.pojo.query.AdminRoleQuery;
import com.doodle.system.controller.pojo.ro.AdminRoleRO;
import com.doodle.system.controller.pojo.vo.AdminRoleVO;
import com.doodle.system.controller.pojo.vo.RoleInfoVO;
import com.doodle.system.service.RoleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin/role")
@Tag(name = "角色管理")
@RequiredArgsConstructor
public class AdminRoleController {

    private final RoleService roleService;

    @GetMapping("/list")
    @Operation(summary = "角色列表")
    @OperateLog(type = OperateTypeEnum.GET)
    public PageResult<AdminRoleVO> getList(@Valid AdminRoleQuery query){

        return roleService.getList(query);
    }

    @GetMapping("/{roleId}")
    @Operation(summary = "角色详情")
    @OperateLog(type = OperateTypeEnum.GET)
    public RoleInfoVO info(@PathVariable Integer roleId) {

        return roleService.getInfo(roleId);
    }

    @PutMapping("/{roleId}")
    @Operation(summary = "角色修改")
    @OperateLog(type = OperateTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('system:role:edit')")
    @NotRepeatSubmit
    public void update(@PathVariable Integer roleId,
                       @Valid @RequestBody AdminRoleRO ro) {

        roleService.updateRole(roleId, ro);
    }

}
