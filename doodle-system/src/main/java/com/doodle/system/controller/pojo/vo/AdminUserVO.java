package com.doodle.system.controller.pojo.vo;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.doodle.framework.common.constant.Constants;
import com.doodle.framework.security.enums.RoleEnum;
import com.doodle.system.enums.UserStatusEnum;
import lombok.Data;
import java.util.Date;
import java.util.List;

@Data
public class AdminUserVO {
    private Long userId;
    private String username;
    private String nickname;
    private String avatar;
    private String phone;
    private String email;
    private Integer sex;
    private UserStatusEnum status;
    private List<RoleEnum> roles;
    @JsonFormat(pattern = Constants.DATE_TIME_PATTERN)
    private Date createTime;
    @JsonFormat(pattern = Constants.DATE_TIME_PATTERN)
    private Date updateTime;
}
