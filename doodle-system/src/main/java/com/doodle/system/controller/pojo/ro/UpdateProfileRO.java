package com.doodle.system.controller.pojo.ro;


import com.doodle.system.enums.SexEnum;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

@Data
public class UpdateProfileRO {
    @NotBlank
    @Length(max = 20)
    private String nickname;
    @Length(max = 11)
    private String phone;
    @Length(max = 50)
    private String email;
    @NotNull
    private SexEnum sex;
}
