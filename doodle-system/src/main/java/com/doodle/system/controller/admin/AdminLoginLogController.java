package com.doodle.system.controller.admin;

import com.doodle.framework.common.model.PageResult;
import com.doodle.framework.operatelog.annotations.OperateLog;
import com.doodle.framework.operatelog.enums.OperateTypeEnum;
import com.doodle.system.controller.pojo.query.LoginLogQuery;
import com.doodle.system.controller.pojo.vo.LoginLogVO;
import com.doodle.system.service.LoginLogService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin/log/login")
@Tag(name = "登录日志")
@RequiredArgsConstructor
public class AdminLoginLogController {
    private final LoginLogService loginLogService;

    @GetMapping("/list")
    @Operation(summary = "日志列表")
    @OperateLog(type = OperateTypeEnum.GET)
    public PageResult<LoginLogVO> list(@Valid LoginLogQuery query){

        return loginLogService.getList(query);
    }

    @DeleteMapping("/{loginIds}")
    @Operation(summary = "删除日志")
    @OperateLog(type = OperateTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('system:log:del')")
    public void delete(@PathVariable Long[] loginIds){

        loginLogService.deleteLog(loginIds);
    }

    @DeleteMapping("/clear")
    @Operation(summary = "清空日志")
    @OperateLog(type = OperateTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('system:log:del')")
    public void clear(){

        loginLogService.clearLog();
    }

    @PostMapping("/export")
    @Operation(summary = "日志导出")
    @OperateLog(type = OperateTypeEnum.EXPORT)
    @PreAuthorize("hasAuthority('system:log:export')")
    public void export(@RequestBody LoginLogQuery query){
        query.setPageSize(Integer.MAX_VALUE);
        loginLogService.export(query);
    }
}
