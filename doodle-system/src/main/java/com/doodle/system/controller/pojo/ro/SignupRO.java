package com.doodle.system.controller.pojo.ro;


import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

@Data
public class SignupRO {
    @Schema(description = "用户名")
    @NotBlank
    @Length(min = 4, max = 20)
    private String username;
    @Schema(description = "密码")
    @NotBlank
    @Length(min = 4, max = 20)
    private String password;
    @Schema(description = "验证码")
    @NotBlank
    private String code;
    @Schema(description = "验证码唯一标识")
    @NotBlank
    private String uuid;
}
