package com.doodle.system.controller.pojo.vo;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.doodle.framework.common.constant.Constants;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Date;

@Data
public class AdminDictDataVO {
    @Schema(description = "数据ID")
    private Long id;
    @Schema(description = "字典ID")
    private Long dictId;
    private String dictType;
    private Integer dictCode;
    private String dictLabel;
    private String dictValue;
    private Integer sort;
    @JsonFormat(pattern = Constants.DATE_TIME_PATTERN)
    private Date createTime;
    @JsonFormat(pattern = Constants.DATE_TIME_PATTERN)
    private Date updateTime;
}
