package com.doodle.system.controller.pojo.query;

import com.doodle.framework.common.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class AdminRoleQuery extends PageParam {
    @Schema(description = "角色名称")
    private String roleName;
    @Schema(description = "权限字符串")
    private String authName;
}
