package com.doodle.system.controller.admin;


import com.doodle.framework.operatelog.annotations.OperateLog;
import com.doodle.framework.operatelog.enums.OperateTypeEnum;
import com.doodle.system.controller.pojo.vo.AdminAuthTreeVO;
import com.doodle.system.service.AuthService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/admin/auth")
@Tag(name = "权限管理")
@RequiredArgsConstructor
public class AdminAuthController {

    private final AuthService authService;

    @GetMapping("/tree")
    @Operation(summary = "权限树")
    @OperateLog(type = OperateTypeEnum.GET)
    public List<AdminAuthTreeVO> tree() {

        return authService.getTree();
    }
}
