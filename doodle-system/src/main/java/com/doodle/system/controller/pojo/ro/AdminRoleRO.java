package com.doodle.system.controller.pojo.ro;

import com.doodle.framework.common.annotation.validator.NonNullElements;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import java.util.Set;

@Data
public class AdminRoleRO {
    @NotNull
    @Range(min = 1, max = 1000)
    private Integer sort;
    @NonNullElements
    private Set<Integer> authIds;
}
