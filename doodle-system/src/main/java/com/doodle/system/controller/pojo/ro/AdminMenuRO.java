package com.doodle.system.controller.pojo.ro;


import com.doodle.system.enums.MenuTypeEnum;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

@Data
public class AdminMenuRO {
    @NotNull
    private MenuTypeEnum menuType;
    @NotBlank
    @Length(max = 50)
    private String title;
    @Length(max = 100)
    private String icon;
    @NotNull
    private Long parentId;
    @NotNull
    @Range(min = 1, max = 1000)
    private Integer sort;
    @Length(max = 50)
    private String name;
    @NotBlank
    @Length(max = 200)
    private String path;
    @Length(max = 200)
    private String query;
    @Length(max = 200)
    private String component;
    @NotNull
    private Boolean isExternal;
}
