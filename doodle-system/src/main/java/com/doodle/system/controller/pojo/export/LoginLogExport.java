package com.doodle.system.controller.pojo.export;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fhs.core.trans.anno.Trans;
import com.fhs.core.trans.constant.TransType;
import com.fhs.core.trans.vo.TransPojo;
import com.doodle.framework.common.excel.DateConverter;
import lombok.Data;

import java.util.Date;

@Data
public class LoginLogExport implements TransPojo {
    @ExcelIgnore
    private Long id;
    @ExcelProperty("用户名")
    private String username;
    @ExcelProperty("登录IP")
    private String ip;
    @ExcelProperty("登录地点")
    private String address;
    @ExcelProperty("UserAgent")
    private String userAgent;
    @ExcelIgnore
    @Trans(type = TransType.DICTIONARY, key = "sys_success_fail", ref = "statusLabel")
    private Integer status;
    @ExcelProperty("登录状态")
    private String statusLabel;
    @ExcelProperty(value = "登录时间", converter = DateConverter.class)
    private Date createTime;
}
