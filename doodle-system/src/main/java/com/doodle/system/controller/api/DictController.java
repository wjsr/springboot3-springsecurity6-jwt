package com.doodle.system.controller.api;


import com.doodle.framework.operatelog.annotations.OperateLog;
import com.doodle.framework.operatelog.enums.OperateTypeEnum;
import com.doodle.system.controller.pojo.vo.DictDataVO;
import com.doodle.system.service.DictDataService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/dict")
@RequiredArgsConstructor
@Tag(name = "字典相关")
public class DictController {

    private final DictDataService dictDataService;

    @GetMapping(value = "/type/{dictType}/data")
    @Operation(summary = "根据字典类型获取字典数据")
    @OperateLog(type = OperateTypeEnum.GET)
    public List<DictDataVO> getDictDataByType(@PathVariable String dictType) {

        return dictDataService.getDictDataByType(dictType);
    }


}
