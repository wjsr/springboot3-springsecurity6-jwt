package com.doodle.system.controller.pojo.vo;


import lombok.Data;

@Data
public class CaptchaVO {
    private String uuid;
    private String img;
}
