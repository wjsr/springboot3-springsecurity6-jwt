package com.doodle.system.controller.pojo.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.doodle.framework.common.constant.Constants;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class AdminMenuVO {
    @Schema(description = "菜单ID")
    private Long menuId;
    @Schema(description = "上级菜单ID")
    private Long parentId;
    @Schema(description = "菜单类型")
    private Integer menuType;
    @Schema(description = "菜单标题")
    private String title;
    @Schema(description = "菜单图标")
    private String icon;
    @Schema(description = "路由名称")
    private String name;
    @Schema(description = "路由地址")
    private String path;
    @Schema(description = "路由参数")
    private String query;
    @Schema(description = "组件路径")
    private String component;
    @Schema(description = "是否外链")
    private Boolean isExternal;
    @Schema(description = "排序")
    private Integer sort;
    @JsonFormat(pattern = Constants.DATE_TIME_PATTERN)
    private Date createTime;
    @JsonFormat(pattern = Constants.DATE_TIME_PATTERN)
    private Date updateTime;
    @Schema(description = "子菜单")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<AdminMenuVO> children;
}
