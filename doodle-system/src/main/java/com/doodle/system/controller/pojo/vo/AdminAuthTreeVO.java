package com.doodle.system.controller.pojo.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

@Data
public class AdminAuthTreeVO {
    private Integer id;
    private String name;
    @Schema(description = "层级 1-分类 2-权限")
    private Integer level;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<AdminAuthTreeVO> children;
}
