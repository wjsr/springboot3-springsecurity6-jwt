package com.doodle.system.controller.admin;


import com.doodle.framework.common.model.PageResult;
import com.doodle.framework.operatelog.annotations.OperateLog;
import com.doodle.framework.operatelog.enums.OperateTypeEnum;
import com.doodle.system.controller.pojo.query.OperateLogQuery;
import com.doodle.system.controller.pojo.vo.OperateLogVO;
import com.doodle.system.service.OperateLogService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin/log/operate")
@Tag(name = "操作日志")
@RequiredArgsConstructor
public class AdminOperateLogController {

    private final OperateLogService operateLogService;

    @GetMapping("/list")
    @Operation(summary = "日志列表")
    @OperateLog(type = OperateTypeEnum.GET)
    public PageResult<OperateLogVO> list(@Valid OperateLogQuery query){

        return operateLogService.getList(query);
    }

    @GetMapping("/{operateId}")
    @Operation(summary = "日志详情")
    @OperateLog(type = OperateTypeEnum.GET)
    public OperateLogVO info(@PathVariable Long operateId){

        return operateLogService.getInfo(operateId);
    }

    @DeleteMapping("/{operateIds}")
    @Operation(summary = "删除日志")
    @OperateLog(type = OperateTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('system:log:del')")
    public void delete(@PathVariable Long[] operateIds){

        operateLogService.deleteLog(operateIds);
    }

    @DeleteMapping("/clear")
    @Operation(summary = "清空日志")
    @OperateLog(type = OperateTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('system:log:del')")
    public void clear(){

        operateLogService.clearLog();
    }

    @PostMapping("/export")
    @Operation(summary = "日志导出")
    @OperateLog(type = OperateTypeEnum.EXPORT)
    @PreAuthorize("hasAuthority('system:log:export')")
    public void export(@RequestBody OperateLogQuery query){
        query.setPageSize(Integer.MAX_VALUE);
        operateLogService.export(query);
    }
}
