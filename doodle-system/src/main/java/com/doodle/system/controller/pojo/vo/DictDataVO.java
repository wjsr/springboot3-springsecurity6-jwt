package com.doodle.system.controller.pojo.vo;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class DictDataVO {
    @Schema(description = "字典数据ID")
    private Long id;
    @Schema(description = "字典类型")
    private String dictType;
    @Schema(description = "数据标签")
    private String dictLabel;
    @Schema(description = "数据编码")
    private Integer dictCode;
    @Schema(description = "数据值")
    private String dictValue;
    @Schema(description = "排序")
    private Integer sort;
}
