package com.doodle.system.controller.api;


import com.google.code.kaptcha.Producer;
import com.doodle.framework.common.annotation.AnonApi;
import com.doodle.framework.common.annotation.aop.NotRepeatSubmit;
import com.doodle.framework.cache.RedisCache;
import com.doodle.framework.cache.RedisKeys;
import com.doodle.framework.common.constant.Constants;
import com.doodle.framework.storage.service.StorageService;
import com.doodle.framework.common.exception.CustomException;
import com.doodle.framework.common.exception.ErrorEnum;
import com.doodle.framework.operatelog.annotations.OperateLog;
import com.doodle.framework.operatelog.enums.OperateTypeEnum;
import com.doodle.framework.security.utils.SecurityUtils;
import com.doodle.system.controller.pojo.ro.LoginRO;
import com.doodle.system.controller.pojo.ro.SignupRO;
import com.doodle.system.controller.pojo.ro.UpdateProfileRO;
import com.doodle.system.controller.pojo.ro.UpdatePwdRO;
import com.doodle.system.controller.pojo.vo.CaptchaVO;
import com.doodle.system.controller.pojo.vo.FileUploadVO;
import com.doodle.system.controller.pojo.vo.TokenVO;
import com.doodle.system.controller.pojo.vo.UserVO;
import com.doodle.system.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.util.FastByteArrayOutputStream;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.UUID;


@RestController
@RequestMapping("/api/user")
@RequiredArgsConstructor
@Tag(name = "用户相关")
public class UserController {

    private final UserService userService;
    private final Producer mathCaptchaProducer;
    private final StorageService storageService;
    private final RedisCache redisCache;


    @PostMapping("/signup")
    @Operation(summary = "注册")
    @OperateLog(type = OperateTypeEnum.SIGNUP)
    @NotRepeatSubmit
    @AnonApi
    public void signup(@Valid @RequestBody SignupRO ro){
        ro.setUsername(ro.getUsername().strip());
        ro.setPassword(ro.getPassword().strip());
        userService.signup(ro);
    }


    @PostMapping("/login")
    @Operation(summary = "登录")
    @NotRepeatSubmit
    @AnonApi
    public TokenVO login(@Valid @RequestBody LoginRO ro){
        ro.setUsername(ro.getUsername().strip());
        ro.setPassword(ro.getPassword().strip());
        return userService.login(ro);
    }

    @GetMapping("/info")
    @Operation(summary = "获取用户信息")
    @OperateLog(type = OperateTypeEnum.GET)
    public UserVO info(){
        return userService.getInfo();
    }


    @PostMapping("/logout")
    @Operation(summary = "退出登录")
    @OperateLog(type = OperateTypeEnum.LOGOUT)
    public void logout(){
        userService.logout();
    }


    @GetMapping("/captcha")
    @Operation(summary = "获取验证码")
    @OperateLog(type = OperateTypeEnum.GET)
    @AnonApi
    public CaptchaVO getCaptcha() {

        // 生成验证码
        String capText = mathCaptchaProducer.createText();
        String capStr = capText.substring(0, capText.lastIndexOf("@"));
        String code = capText.substring(capText.lastIndexOf("@") + 1);
        BufferedImage image = mathCaptchaProducer.createImage(capStr);
        String uuid = UUID.randomUUID().toString();
        String key = RedisKeys.getCaptchaCodeKey(uuid);
        redisCache.set(key, code, Constants.CAPTCHA_EXPIRATION);

        // 转换流信息写出
        try (FastByteArrayOutputStream os = new FastByteArrayOutputStream()) {
            ImageIO.write(image,"jpg", os);
            CaptchaVO captchaVO = new CaptchaVO();
            captchaVO.setUuid(uuid);
            captchaVO.setImg(Base64.getEncoder().encodeToString(os.toByteArray()));
            return captchaVO;
        } catch (Exception e) {
            redisCache.delete(key);
            throw CustomException.of(ErrorEnum.GENERATE_CAPTCHA_FAILURE);
        }
    }


    @PostMapping("/avatar")
    @Operation(summary = "用户头像上传")
    @OperateLog(type = OperateTypeEnum.UPLOAD)
    @NotRepeatSubmit
    public FileUploadVO avatar(@RequestParam MultipartFile file) throws IOException {
        if (file.isEmpty()) {
            throw CustomException.of("请选择需要上传的文件");
        }
        // 校验是否是图片
        try (InputStream fin = file.getInputStream()) {
            ImageIO.read(fin);
        } catch (Exception e) {
            throw CustomException.of("文件不是图片类型");
        }
        // 上传路径
        String path = storageService.getPath(file.getOriginalFilename());
        // 上传文件
        String url = storageService.upload(file.getBytes(), path);
        // 更新用户头像
        userService.updateUserAvatar(SecurityUtils.getUserId(), url);

        FileUploadVO vo = new FileUploadVO();
        vo.setUrl(url);
        vo.setSize(file.getSize());
        vo.setName(file.getOriginalFilename());

        return vo;
    }

    @PutMapping("/profile")
    @Operation(summary = "用户修改个人基本信息")
    @OperateLog(type = OperateTypeEnum.UPDATE)
    @NotRepeatSubmit
    public void updateProfile(@Valid @RequestBody UpdateProfileRO ro) {

        userService.updateUserProfile(SecurityUtils.getUserId(), ro);
    }

    @PatchMapping("/password")
    @Operation(summary = "用户重置密码")
    @OperateLog(type = OperateTypeEnum.UPDATE)
    @NotRepeatSubmit
    public void updatePwd(@Valid @RequestBody UpdatePwdRO ro) {

        userService.updateUserPwd(SecurityUtils.getUserId(), ro);
    }


}
