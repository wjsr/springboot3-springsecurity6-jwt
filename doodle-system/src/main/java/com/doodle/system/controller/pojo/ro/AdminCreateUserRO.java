package com.doodle.system.controller.pojo.ro;


import com.doodle.system.enums.SexEnum;
import com.doodle.system.enums.UserStatusEnum;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

@Data
public class AdminCreateUserRO {
    @Schema(description = "用户名")
    @NotBlank
    @Length(min = 4, max = 20)
    private String username;
    @NotBlank
    @Length(max = 20)
    private String nickname;
    private String phone;
    private String email;
    @NotNull
    private SexEnum sex;
    @NotNull
    private UserStatusEnum status;
}
