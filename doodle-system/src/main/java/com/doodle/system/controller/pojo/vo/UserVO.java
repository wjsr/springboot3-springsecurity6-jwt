package com.doodle.system.controller.pojo.vo;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.doodle.framework.common.constant.Constants;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class UserVO {
    private Long userId;
    private String username;
    private String nickname;
    private String avatar;
    private String phone;
    private String email;
    private Integer sex;
    private List<String> roles;
    private List<String> auths;
    @JsonFormat(pattern = Constants.DATE_TIME_PATTERN)
    private Date createTime;
}
