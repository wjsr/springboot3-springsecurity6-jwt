package com.doodle.system.controller.pojo.vo;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.doodle.framework.common.constant.Constants;
import lombok.Data;

import java.util.Date;

@Data
public class AdminDictVO {
    private Long dictId;
    private String dictName;
    private String dictType;
    @JsonFormat(pattern = Constants.DATE_TIME_PATTERN)
    private Date createTime;
    @JsonFormat(pattern = Constants.DATE_TIME_PATTERN)
    private Date updateTime;
}
