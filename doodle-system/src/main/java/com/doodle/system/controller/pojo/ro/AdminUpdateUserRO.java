package com.doodle.system.controller.pojo.ro;


import com.doodle.system.enums.SexEnum;
import com.doodle.system.enums.UserStatusEnum;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

@Data
public class AdminUpdateUserRO {
    @NotBlank
    @Length(max = 20)
    private String nickname;
    private String phone;
    private String email;
    @NotNull
    private SexEnum sex;
    @NotNull
    private UserStatusEnum status;
}
