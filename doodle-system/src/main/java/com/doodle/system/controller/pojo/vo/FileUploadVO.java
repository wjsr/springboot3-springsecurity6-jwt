package com.doodle.system.controller.pojo.vo;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
public class FileUploadVO {

    @Schema(description = "文件名称")
    private String name;

    @Schema(description = "文件地址")
    private String url;

    @Schema(description = "文件大小")
    private Long size;
}
