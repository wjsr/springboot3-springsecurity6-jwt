package com.doodle.system.controller.pojo.export;

import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fhs.core.trans.anno.Trans;
import com.fhs.core.trans.constant.TransType;
import com.fhs.core.trans.vo.TransPojo;
import com.doodle.framework.common.excel.DateConverter;
import com.doodle.framework.operatelog.enums.OperateTypeEnum;
import lombok.Data;

import java.util.Date;

@Data
public class OperateLogExport implements TransPojo {
    @ExcelIgnore
    private Long id;
    @ExcelProperty("模块")
    private String module;
    @ExcelProperty("操作")
    private String name;
    @ExcelProperty("请求地址")
    private String reqUri;
    @ExcelProperty("请求方法")
    private String reqMethod;
    @ExcelProperty("请求参数")
    private String reqParams;
    @ExcelProperty("操作IP")
    private String ip;
    @ExcelProperty("操作地址")
    private String address;
    @ExcelProperty("UserAgent")
    private String userAgent;
    @ExcelIgnore
    @Trans(type = TransType.ENUM, key = "label", ref = "operateTypeLabel")
    private OperateTypeEnum operateType;
    @ExcelProperty("操作类型")
    private String operateTypeLabel;
    @ExcelProperty("执行时长")
    private Long duration;
    @ExcelIgnore
    @Trans(type = TransType.DICTIONARY, key = "sys_success_fail", ref = "statusLabel")
    private Integer status;
    @ExcelProperty("操作状态")
    private String statusLabel;
    @ExcelProperty("操作人员")
    private String username;
    @ExcelProperty("返回结果")
    private String resultMsg;
    @ExcelProperty(value = "操作时间", converter = DateConverter.class)
    private Date createTime;
}
