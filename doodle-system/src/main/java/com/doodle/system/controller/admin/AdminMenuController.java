package com.doodle.system.controller.admin;


import com.doodle.framework.common.annotation.aop.NotRepeatSubmit;
import com.doodle.framework.operatelog.annotations.OperateLog;
import com.doodle.framework.operatelog.enums.OperateTypeEnum;
import com.doodle.system.controller.pojo.ro.AdminMenuRO;
import com.doodle.system.controller.pojo.vo.AdminMenuVO;
import com.doodle.system.controller.pojo.vo.RouteVO;
import com.doodle.system.service.MenuService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/admin/menu")
@Tag(name = "菜单管理")
@RequiredArgsConstructor
public class AdminMenuController {

    private final MenuService menuService;

    @GetMapping("/routes")
    @Operation(summary = "获取后台路由")
    @OperateLog(type = OperateTypeEnum.GET)
    public List<RouteVO> routes(){

        return menuService.getRoutes();
    }

    @GetMapping("/list")
    @Operation(summary = "获取菜单列表")
    @OperateLog(type = OperateTypeEnum.GET)
    public List<AdminMenuVO> list(@RequestParam(required = false) String title) {

        return menuService.getList(title);
    }

    @GetMapping("/{menuId}")
    @Operation(summary = "获取菜单信息")
    @OperateLog(type = OperateTypeEnum.GET)
    public AdminMenuVO info(@PathVariable Long menuId) {

        return menuService.getInfo(menuId);
    }

    @PostMapping("")
    @Operation(summary = "创建菜单")
    @OperateLog(type = OperateTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('system:menu:add')")
    @NotRepeatSubmit
    public void addMenu(@Valid @RequestBody AdminMenuRO ro) {

        menuService.addMenu(ro);
    }

    @PutMapping("/{menuId}")
    @Operation(summary = "修改菜单")
    @OperateLog(type = OperateTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('system:menu:edit')")
    @NotRepeatSubmit
    public void updateMenu(@PathVariable Long menuId,
                           @Valid @RequestBody AdminMenuRO ro) {

        menuService.updateMenu(menuId, ro);
    }

    @DeleteMapping("/{menuId}")
    @Operation(summary = "删除菜单")
    @OperateLog(type = OperateTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('system:menu:del')")
    @NotRepeatSubmit
    public void delMenu(@PathVariable Long menuId) {

        menuService.delMenu(menuId);
    }

}
