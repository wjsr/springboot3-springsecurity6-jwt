package com.doodle.system.controller.pojo.vo;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TokenVO {
    private String token;
}
