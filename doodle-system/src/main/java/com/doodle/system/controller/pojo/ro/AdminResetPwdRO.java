package com.doodle.system.controller.pojo.ro;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

@Data
public class AdminResetPwdRO {
    @NotBlank
    @Length(min = 4, max = 20)
    private String password;
}
