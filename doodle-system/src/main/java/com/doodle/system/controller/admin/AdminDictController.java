package com.doodle.system.controller.admin;


import com.doodle.framework.common.annotation.aop.NotRepeatSubmit;
import com.doodle.framework.common.model.PageResult;
import com.doodle.framework.operatelog.annotations.OperateLog;
import com.doodle.framework.operatelog.enums.OperateTypeEnum;
import com.doodle.system.controller.pojo.query.AdminDictQuery;
import com.doodle.system.controller.pojo.ro.AdminDictRO;
import com.doodle.system.controller.pojo.vo.AdminDictVO;
import com.doodle.system.service.DictTypeService;
import com.doodle.system.entity.DictTypeDO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/admin/dict")
@Tag(name = "字典管理")
@RequiredArgsConstructor
public class AdminDictController {

    private final DictTypeService dictTypeService;

    @GetMapping("/list-all")
    @Operation(summary = "所有字典")
    @OperateLog(type = OperateTypeEnum.GET)
    public List<DictTypeDO> listAll(){

        return dictTypeService.list();
    }


    @GetMapping("/list")
    @Operation(summary = "字典列表")
    @OperateLog(type = OperateTypeEnum.GET)
    public PageResult<AdminDictVO> list(@Valid AdminDictQuery query){

        return dictTypeService.getList(query);
    }

    @GetMapping("/{dictId}")
    @Operation(summary = "字典详情")
    @OperateLog(type = OperateTypeEnum.GET)
    public AdminDictVO info(@PathVariable Long dictId){

        return dictTypeService.getInfo(dictId);
    }

    @PostMapping("")
    @Operation(summary = "创建字典")
    @OperateLog(type = OperateTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('system:dict:add')")
    @NotRepeatSubmit
    public void addDict(@Valid @RequestBody AdminDictRO ro){

        dictTypeService.addDict(ro);
    }

    @PutMapping("/{dictId}")
    @Operation(summary = "字典编辑")
    @OperateLog(type = OperateTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('system:dict:edit')")
    @NotRepeatSubmit
    public void update(@PathVariable Long dictId,
                       @Valid @RequestBody AdminDictRO ro) {

        dictTypeService.updateDict(dictId, ro);
    }

    @DeleteMapping("/{dictIds}")
    @Operation(summary = "字典删除")
    @OperateLog(type = OperateTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('system:dict:del')")
    public void delete(@PathVariable Long[] dictIds){

        dictTypeService.deleteDict(dictIds);
    }

    @PutMapping("/refresh")
    @Operation(summary = "刷新字典缓存")
    @OperateLog(type = OperateTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('system:dict:refresh')")
    public void refresh() {

        dictTypeService.refreshTransCache();
    }

}
