package com.doodle.system.controller.admin;


import com.doodle.framework.common.annotation.aop.NotRepeatSubmit;
import com.doodle.framework.common.annotation.validator.NonNullElements;
import com.doodle.framework.common.model.PageResult;
import com.doodle.framework.common.exception.CustomException;
import com.doodle.framework.operatelog.annotations.OperateLog;
import com.doodle.framework.operatelog.enums.OperateTypeEnum;
import com.doodle.framework.security.enums.RoleEnum;
import com.doodle.framework.security.utils.SecurityUtils;
import com.doodle.system.controller.pojo.query.AdminUserQuery;
import com.doodle.system.controller.pojo.ro.AdminCreateUserRO;
import com.doodle.system.controller.pojo.ro.AdminResetPwdRO;
import com.doodle.system.controller.pojo.ro.AdminUpdateUserRO;
import com.doodle.system.controller.pojo.vo.AdminUserVO;
import com.doodle.system.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.Set;


@RestController
@RequestMapping("/admin/user")
@Tag(name = "用户管理")
@RequiredArgsConstructor
public class AdminUserController {

    private final UserService userService;

    @PostMapping("")
    @Operation(summary = "创建用户")
    @OperateLog(type = OperateTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('system:user:add')")
    @NotRepeatSubmit
    public void createUser(@Valid @RequestBody AdminCreateUserRO ro){
        ro.setUsername(ro.getUsername().strip());
        userService.addUser(ro);
    }

    @GetMapping("/list")
    @Operation(summary = "用户列表")
    @OperateLog(type = OperateTypeEnum.GET)
    public PageResult<AdminUserVO> getList(@Valid AdminUserQuery query){

        return userService.getList(query);
    }

    @GetMapping("/{userId}")
    @Operation(summary = "获取用户详情")
    @OperateLog(type = OperateTypeEnum.GET)
    public AdminUserVO getInfo(@PathVariable Long userId) {

        return userService.getInfo(userId);
    }

    @PatchMapping("/{userId}/resetPwd")
    @Operation(summary = "重置用户密码")
    @OperateLog(type = OperateTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('system:user:edit')")
    @NotRepeatSubmit
    public void resetUserPwd(@PathVariable Long userId,
                             @Valid @RequestBody AdminResetPwdRO ro) {

        userService.resetUserPwd(userId, ro.getPassword());
    }

    @PutMapping("/{userId}/info")
    @Operation(summary = "修改用户基本信息")
    @OperateLog(type = OperateTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('system:user:edit')")
    @NotRepeatSubmit
    public void updateUserInfo(@PathVariable Long userId,
                               @Valid @RequestBody AdminUpdateUserRO ro) {

        userService.updateUserInfo(userId, ro);
    }

    @PatchMapping("/{userId}/roles")
    @Operation(summary = "修改用户角色")
    @OperateLog(type = OperateTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('system:role:assign')")
    @NotRepeatSubmit
    public void updateUserRole(@PathVariable Long userId,
                               @RequestBody @NonNullElements Set<RoleEnum> roles) {
        if (userId.equals(SecurityUtils.getUserId())) {
            throw CustomException.of("不能为自身分配角色");
        }
        userService.updateUserRole(userId, roles);
    }

    @PatchMapping("/{userId}/avatar")
    @Operation(summary = "修改用户头像")
    @OperateLog(type = OperateTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('system:user:edit')")
    @NotRepeatSubmit
    public void updateUserAvatar(@PathVariable Long userId,
                                 @RequestParam String url) {
        if (StringUtils.isBlank(url)) {
            throw CustomException.of("请先上传头像");
        }
        userService.updateUserAvatar(userId, url);
    }


    @DeleteMapping("/{userIds}")
    @Operation(summary = "删除用户")
    @OperateLog(type = OperateTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('system:user:del')")
    public void deleteUser(@PathVariable Long[] userIds){

        if (ArrayUtils.contains(userIds, SecurityUtils.getUserId())) {
            throw CustomException.of("不能删除当前用户");
        }

        userService.deleteUser(userIds);
    }

    @PostMapping("/export")
    @Operation(summary = "导出用户数据")
    @OperateLog(type = OperateTypeEnum.EXPORT)
    @PreAuthorize("hasAuthority('system:user:export')")
    public void export(@RequestBody AdminUserQuery query) {
        query.setPageSize(Integer.MAX_VALUE);
        userService.export(query);
    }
}
