package com.doodle.system.controller.pojo.ro;


import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class AdminDictRO {
    @NotBlank
    private String dictName;
    @NotBlank
    private String dictType;
}
