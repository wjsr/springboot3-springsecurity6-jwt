package com.doodle.system.controller.pojo.query;


import com.doodle.framework.common.constant.Constants;
import com.doodle.framework.common.model.PageParam;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;


@Data
@EqualsAndHashCode(callSuper = false)
public class AdminUserQuery extends PageParam {
    private String username;
    private String nickname;
    private String phone;
    private String email;
    private Integer sex;
    private Integer status;
    @DateTimeFormat(pattern = Constants.DATE_PATTERN)
    private Date beginTime;
    @DateTimeFormat(pattern = Constants.DATE_PATTERN)
    private Date endTime;
}
