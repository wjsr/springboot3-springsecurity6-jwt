package com.doodle.system.controller.pojo.vo;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

@Data
public class RoleInfoVO {
    private Integer roleId;
    private String roleName;
    private Integer sort;
    @Schema(description = "权限列表")
    private List<Integer> authIds;
}
