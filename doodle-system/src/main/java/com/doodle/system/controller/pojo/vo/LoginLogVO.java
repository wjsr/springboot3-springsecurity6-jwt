package com.doodle.system.controller.pojo.vo;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.doodle.framework.common.constant.Constants;
import lombok.Data;

import java.util.Date;

@Data
public class LoginLogVO {
    private Long loginId;
    private String username;
    private String ip;
    private String address;
    private String userAgent;
    private Integer status;
    @JsonFormat(pattern = Constants.DATE_TIME_PATTERN)
    private Date createTime;
}
