package com.doodle.system.controller.pojo.vo;


import com.fasterxml.jackson.annotation.JsonFormat;
import com.doodle.framework.common.constant.Constants;
import lombok.Data;

import java.util.Date;

@Data
public class AdminRoleVO {
    private Integer roleId;
    private Integer roleCode;
    private String roleName;
    private Integer sort;
    @JsonFormat(pattern = Constants.DATE_TIME_PATTERN)
    private Date createTime;
    @JsonFormat(pattern = Constants.DATE_TIME_PATTERN)
    private Date updateTime;
}
