package com.doodle.system.controller.pojo.query;


import com.doodle.framework.common.constant.Constants;
import com.doodle.framework.common.model.PageParam;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;


@Data
@EqualsAndHashCode(callSuper = false)
public class OperateLogQuery extends PageParam {
    @Schema(description = "IP地址")
    private String ip;
    @Schema(description = "系统模块")
    private String module;
    @Schema(description = "操作人员")
    private String operator;
    @Schema(description = "操作类型")
    private Integer operateType;
    @Schema(description = "状态")
    private Integer status;
    @DateTimeFormat(pattern = Constants.DATE_PATTERN)
    private Date beginTime;
    @DateTimeFormat(pattern = Constants.DATE_PATTERN)
    private Date endTime;
}
