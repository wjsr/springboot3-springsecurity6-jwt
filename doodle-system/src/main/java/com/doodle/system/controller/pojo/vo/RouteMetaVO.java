package com.doodle.system.controller.pojo.vo;


import lombok.Data;

@Data
public class RouteMetaVO {
    private String title;
    private String icon;
}
