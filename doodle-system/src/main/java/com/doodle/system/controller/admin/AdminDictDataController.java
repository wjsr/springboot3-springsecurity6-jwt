package com.doodle.system.controller.admin;


import com.doodle.framework.common.annotation.aop.NotRepeatSubmit;
import com.doodle.framework.common.model.PageResult;
import com.doodle.framework.operatelog.annotations.OperateLog;
import com.doodle.framework.operatelog.enums.OperateTypeEnum;
import com.doodle.system.controller.pojo.query.AdminDictDataQuery;
import com.doodle.system.controller.pojo.ro.AdminDictDataRO;
import com.doodle.system.controller.pojo.vo.AdminDictDataVO;
import com.doodle.system.service.DictDataService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/admin/dict/data")
@Tag(name = "字典数据管理")
@RequiredArgsConstructor
public class AdminDictDataController {

    private final DictDataService dictDataService;

    @GetMapping("/list")
    @Operation(summary = "数据列表")
    @OperateLog(type = OperateTypeEnum.GET)
    public PageResult<AdminDictDataVO> list(@Valid AdminDictDataQuery query){

        return dictDataService.getList(query);
    }

    @GetMapping("/{id}")
    @Operation(summary = "数据详情")
    @OperateLog(type = OperateTypeEnum.GET)
    public AdminDictDataVO info(@PathVariable Long id){

        return dictDataService.getInfo(id);
    }

    @PostMapping("")
    @Operation(summary = "创建数据")
    @OperateLog(type = OperateTypeEnum.INSERT)
    @PreAuthorize("hasAuthority('system:dict:add')")
    @NotRepeatSubmit
    public void add(@Valid @RequestBody AdminDictDataRO ro){

        dictDataService.addData(ro);
    }

    @PutMapping("/{id}")
    @Operation(summary = "数据编辑")
    @OperateLog(type = OperateTypeEnum.UPDATE)
    @PreAuthorize("hasAuthority('system:dict:edit')")
    @NotRepeatSubmit
    public void update(@PathVariable Long id,
                       @Valid @RequestBody AdminDictDataRO ro) {

        dictDataService.updateData(id, ro);
    }

    @DeleteMapping("/{ids}")
    @Operation(summary = "数据删除")
    @OperateLog(type = OperateTypeEnum.DELETE)
    @PreAuthorize("hasAuthority('system:dict:del')")
    public void delete(@PathVariable Long[] ids){

        dictDataService.deleteData(ids);
    }
}
