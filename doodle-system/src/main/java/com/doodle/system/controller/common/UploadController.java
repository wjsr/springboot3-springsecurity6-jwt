package com.doodle.system.controller.common;


import com.doodle.framework.storage.service.StorageService;
import com.doodle.framework.common.exception.CustomException;
import com.doodle.framework.operatelog.annotations.OperateLog;
import com.doodle.framework.operatelog.enums.OperateTypeEnum;
import com.doodle.system.controller.pojo.vo.FileUploadVO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("/common/upload")
@Tag(name = "文件管理")
@RequiredArgsConstructor
public class UploadController {

    private final StorageService storageService;

    @PostMapping("")
    @Operation(summary = "上传文件")
    @OperateLog(type = OperateTypeEnum.UPLOAD)
    public FileUploadVO createUser(@RequestParam MultipartFile file) throws IOException {
        if (file.isEmpty()) {
            throw CustomException.of("请选择需要上传的文件");
        }
        // 上传路径
        String path = storageService.getPath(file.getOriginalFilename());
        // 上传文件
        String url = storageService.upload(file.getBytes(), path);

        FileUploadVO vo = new FileUploadVO();
        vo.setUrl(url);
        vo.setSize(file.getSize());
        vo.setName(file.getOriginalFilename());

        return vo;
    }

}
