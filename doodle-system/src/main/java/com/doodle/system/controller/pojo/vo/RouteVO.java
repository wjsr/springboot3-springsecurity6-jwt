package com.doodle.system.controller.pojo.vo;


import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;


@Data
public class RouteVO {
    private String name;
    private String path;
    private String query;
    private String component;
    private Boolean isExternal;
    private Integer menuType;
    private RouteMetaVO meta;
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<RouteVO> children;
}
