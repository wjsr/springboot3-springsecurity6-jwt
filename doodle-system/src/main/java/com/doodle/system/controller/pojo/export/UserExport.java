package com.doodle.system.controller.pojo.export;


import com.alibaba.excel.annotation.ExcelIgnore;
import com.alibaba.excel.annotation.ExcelProperty;
import com.fhs.core.trans.anno.Trans;
import com.fhs.core.trans.constant.TransType;
import com.fhs.core.trans.vo.TransPojo;
import com.doodle.framework.common.excel.DateConverter;
import com.doodle.system.enums.UserStatusEnum;
import lombok.Data;

import java.util.Date;

@Data
public class UserExport implements TransPojo {
    /**
     * 必须，翻译时作为 transMap 中的 key，需唯一
     * 或者定义 private Map<String, Object> transMap = new HashMap<>(); 来代替 id
     */
    @ExcelIgnore
    private Long id;
    @ExcelProperty("用户名")
    private String username;
    @ExcelProperty("昵称")
    private String nickname;
    @ExcelProperty("头像地址")
    private String avatar;
    @ExcelProperty("电话")
    private String phone;
    @ExcelProperty("邮箱")
    private String email;
    @ExcelIgnore
    @Trans(type = TransType.DICTIONARY, key = "sys_user_sex", ref = "sexLabel")
    private Integer sex;
    @ExcelProperty("性别")
    private String sexLabel;
    @ExcelIgnore
    @Trans(type = TransType.ENUM, key = "label", ref = "statusLabel")
    private UserStatusEnum status;
    @ExcelProperty("用户状态")
    private String statusLabel;
    @ExcelProperty("所属角色")
    private String roleLabel;
    @ExcelProperty(value = "创建时间", converter = DateConverter.class)
    private Date createTime;
    @ExcelProperty(value = "修改时间", converter = DateConverter.class)
    private Date updateTime;
}
