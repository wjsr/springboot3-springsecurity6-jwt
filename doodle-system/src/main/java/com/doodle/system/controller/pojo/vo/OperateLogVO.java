package com.doodle.system.controller.pojo.vo;


import com.fasterxml.jackson.annotation.JsonFormat;

import com.doodle.framework.common.constant.Constants;
import com.doodle.framework.operatelog.enums.OperateTypeEnum;
import lombok.Data;

import java.util.Date;

@Data
public class OperateLogVO {
    private Long operateId;
    private String module;
    private String name;
    private String reqUri;
    private String reqMethod;
    private String reqParams;
    private String ip;
    private String address;
    private String userAgent;
    private OperateTypeEnum operateType;
    private Long duration;
    private Integer status;
    private String username;
    private String resultMsg;
    @JsonFormat(pattern = Constants.DATE_TIME_PATTERN)
    private Date createTime;
}
