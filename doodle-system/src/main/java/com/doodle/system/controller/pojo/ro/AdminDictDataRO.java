package com.doodle.system.controller.pojo.ro;


import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
public class AdminDictDataRO {
    @NotBlank
    private String dictType;
    @NotNull
    private Integer dictCode;
    @NotBlank
    private String dictValue;
    @NotBlank
    private String dictLabel;
    @NotNull
    @Min(1)
    private Integer sort;
}
