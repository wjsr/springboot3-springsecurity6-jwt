

CREATE TABLE `t_auth` (
  `auth_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `auth_code` int(11) NOT NULL COMMENT '权限编号',
  `auth_name` varchar(50) NOT NULL COMMENT '权限名称',
  `permission` varchar(50) NOT NULL COMMENT '权限字符串',
  `category_id` int(11) NOT NULL COMMENT '所属分类',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`auth_id`),
  UNIQUE KEY `uk_auth_name` (`auth_name`) USING BTREE,
  UNIQUE KEY `uk_auth_code` (`auth_code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='权限表';

INSERT INTO `t_auth` (`auth_id`, `auth_code`, `auth_name`, `permission`, `category_id`) VALUES (1, 1, '用户添加', 'system:user:add', 1);
INSERT INTO `t_auth` (`auth_id`, `auth_code`, `auth_name`, `permission`, `category_id`) VALUES (2, 2, '用户删除', 'system:user:del', 1);
INSERT INTO `t_auth` (`auth_id`, `auth_code`, `auth_name`, `permission`, `category_id`) VALUES (3, 3, '用户编辑', 'system:user:edit', 1);
INSERT INTO `t_auth` (`auth_id`, `auth_code`, `auth_name`, `permission`, `category_id`) VALUES (4, 4, '用户导出', 'system:user:export', 1);
INSERT INTO `t_auth` (`auth_id`, `auth_code`, `auth_name`, `permission`, `category_id`) VALUES (5, 5, '角色分配', 'system:role:assign', 2);
INSERT INTO `t_auth` (`auth_id`, `auth_code`, `auth_name`, `permission`, `category_id`) VALUES (6, 6, '角色编辑', 'system:role:edit', 2);
INSERT INTO `t_auth` (`auth_id`, `auth_code`, `auth_name`, `permission`, `category_id`) VALUES (7, 7, '菜单添加', 'system:menu:add', 3);
INSERT INTO `t_auth` (`auth_id`, `auth_code`, `auth_name`, `permission`, `category_id`) VALUES (8, 8, '菜单编辑', 'system:menu:edit', 3);
INSERT INTO `t_auth` (`auth_id`, `auth_code`, `auth_name`, `permission`, `category_id`) VALUES (9, 9, '菜单删除', 'system:menu:del', 3);
INSERT INTO `t_auth` (`auth_id`, `auth_code`, `auth_name`, `permission`, `category_id`) VALUES (10, 10, '字典添加', 'system:dict:add', 4);
INSERT INTO `t_auth` (`auth_id`, `auth_code`, `auth_name`, `permission`, `category_id`) VALUES (11, 11, '字典编辑', 'system:dict:edit', 4);
INSERT INTO `t_auth` (`auth_id`, `auth_code`, `auth_name`, `permission`, `category_id`) VALUES (12, 12, '字典删除', 'system:dict:del', 4);
INSERT INTO `t_auth` (`auth_id`, `auth_code`, `auth_name`, `permission`, `category_id`) VALUES (13, 13, '刷新字典缓存', 'system:dict:refresh', 4);
INSERT INTO `t_auth` (`auth_id`, `auth_code`, `auth_name`, `permission`, `category_id`) VALUES (14, 14, '日志删除', 'system:log:del', 5);
INSERT INTO `t_auth` (`auth_id`, `auth_code`, `auth_name`, `permission`, `category_id`) VALUES (15, 15, '日志导出', 'system:log:export', 5);

CREATE TABLE `t_auth_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT COMMENT '分类ID',
  `name` varchar(50) NOT NULL COMMENT '分类名',
  `sort` int(11) NOT NULL COMMENT '排序',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `uk` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='权限分类表';

INSERT INTO `t_auth_category` (`category_id`, `name`, `sort`) VALUES (1, '用户相关', 1);
INSERT INTO `t_auth_category` (`category_id`, `name`, `sort`) VALUES (2, '角色相关', 2);
INSERT INTO `t_auth_category` (`category_id`, `name`, `sort`) VALUES (3, '菜单相关', 3);
INSERT INTO `t_auth_category` (`category_id`, `name`, `sort`) VALUES (4, '字典相关', 4);
INSERT INTO `t_auth_category` (`category_id`, `name`, `sort`) VALUES (5, '日志相关', 5);


CREATE TABLE `t_dict_type` (
  `dict_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '字典ID',
  `dict_name` varchar(100) DEFAULT NULL COMMENT '字典名称',
  `dict_type` varchar(100) DEFAULT NULL COMMENT '字典类型',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`dict_id`),
  UNIQUE KEY `uk_type` (`dict_type`),
  UNIQUE KEY `uk_name` (`dict_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='字典类型表';


INSERT INTO `t_dict_type` (`dict_id`, `dict_name`, `dict_type`) VALUES (1, '用户性别', 'sys_user_sex');
INSERT INTO `t_dict_type` (`dict_id`, `dict_name`, `dict_type`) VALUES (2, '系统角色', 'sys_user_role');
INSERT INTO `t_dict_type` (`dict_id`, `dict_name`, `dict_type`) VALUES (3, '菜单类型', 'sys_menu_type');
INSERT INTO `t_dict_type` (`dict_id`, `dict_name`, `dict_type`) VALUES (4, '用户状态', 'sys_user_status');
INSERT INTO `t_dict_type` (`dict_id`, `dict_name`, `dict_type`) VALUES (5, '操作类型', 'sys_operate_type');
INSERT INTO `t_dict_type` (`dict_id`, `dict_name`, `dict_type`) VALUES (6, '成功与否', 'sys_success_fail');

CREATE TABLE `t_dict_data` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `dict_id` bigint(20) NOT NULL COMMENT '字典ID',
  `dict_label` varchar(100) NOT NULL COMMENT '字典标签',
  `dict_code` int(11) NOT NULL COMMENT '字典编码',
  `dict_value` varchar(100) NOT NULL COMMENT '字典键值',
  `sort` int(4) DEFAULT NULL COMMENT '排序',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `uk` (`dict_id`,`dict_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='字典数据表';

INSERT INTO `t_dict_data` (`dict_id`, `dict_label`, `dict_code`, `dict_value`, `sort`) VALUES (1, '男', 1, 'male', 1);
INSERT INTO `t_dict_data` (`dict_id`, `dict_label`, `dict_code`, `dict_value`, `sort`) VALUES (1, '女', 2, 'female', 2);
INSERT INTO `t_dict_data` (`dict_id`, `dict_label`, `dict_code`, `dict_value`, `sort`) VALUES (1, '未知', 3, 'unknown', 3);
INSERT INTO `t_dict_data` (`dict_id`, `dict_label`, `dict_code`, `dict_value`, `sort`) VALUES (2, '普通用户', 1, 'user', 3);
INSERT INTO `t_dict_data` (`dict_id`, `dict_label`, `dict_code`, `dict_value`, `sort`) VALUES (2, '管理员', 2, 'admin', 2);
INSERT INTO `t_dict_data` (`dict_id`, `dict_label`, `dict_code`, `dict_value`, `sort`) VALUES (2, '超级管理员', 3, 'super_admin', 1);
INSERT INTO `t_dict_data` (`dict_id`, `dict_label`, `dict_code`, `dict_value`, `sort`) VALUES (3, '目录', 1, 'catalog', 1);
INSERT INTO `t_dict_data` (`dict_id`, `dict_label`, `dict_code`, `dict_value`, `sort`) VALUES (3, '菜单', 2, 'menu', 2);
INSERT INTO `t_dict_data` (`dict_id`, `dict_label`, `dict_code`, `dict_value`, `sort`) VALUES (4, '正常', 1, 'normal', 1);
INSERT INTO `t_dict_data` (`dict_id`, `dict_label`, `dict_code`, `dict_value`, `sort`) VALUES (4, '禁用', 2, 'disable', 2);
INSERT INTO `t_dict_data` (`dict_id`, `dict_label`, `dict_code`, `dict_value`, `sort`) VALUES (4, '注销', 3, 'deleted', 3);
INSERT INTO `t_dict_data` (`dict_id`, `dict_label`, `dict_code`, `dict_value`, `sort`) VALUES (5, '查询', 1, 'get', 1);
INSERT INTO `t_dict_data` (`dict_id`, `dict_label`, `dict_code`, `dict_value`, `sort`) VALUES (5, '新增', 2, 'insert', 2);
INSERT INTO `t_dict_data` (`dict_id`, `dict_label`, `dict_code`, `dict_value`, `sort`) VALUES (5, '修改', 3, 'update', 3);
INSERT INTO `t_dict_data` (`dict_id`, `dict_label`, `dict_code`, `dict_value`, `sort`) VALUES (5, '删除', 4, 'delete', 4);
INSERT INTO `t_dict_data` (`dict_id`, `dict_label`, `dict_code`, `dict_value`, `sort`) VALUES (5, '导出', 5, 'export', 5);
INSERT INTO `t_dict_data` (`dict_id`, `dict_label`, `dict_code`, `dict_value`, `sort`) VALUES (5, '导入', 6, 'import', 6);
INSERT INTO `t_dict_data` (`dict_id`, `dict_label`, `dict_code`, `dict_value`, `sort`) VALUES (5, '退出登录', 7, 'logout', 7);
INSERT INTO `t_dict_data` (`dict_id`, `dict_label`, `dict_code`, `dict_value`, `sort`) VALUES (5, '注册', 8, 'signup', 8);
INSERT INTO `t_dict_data` (`dict_id`, `dict_label`, `dict_code`, `dict_value`, `sort`) VALUES (5, '上传', 9, 'upload', 9);
INSERT INTO `t_dict_data` (`dict_id`, `dict_label`, `dict_code`, `dict_value`, `sort`) VALUES (5, '下载', 10, 'download', 10);
INSERT INTO `t_dict_data` (`dict_id`, `dict_label`, `dict_code`, `dict_value`, `sort`) VALUES (5, '其它', 0, 'other', 11);
INSERT INTO `t_dict_data` (`dict_id`, `dict_label`, `dict_code`, `dict_value`, `sort`) VALUES (6, '成功', 1, 'success', 1);
INSERT INTO `t_dict_data` (`dict_id`, `dict_label`, `dict_code`, `dict_value`, `sort`) VALUES (6, '失败', 0, 'fail', 2);



CREATE TABLE `t_role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `role_code` int(11) NOT NULL COMMENT '角色编号',
  `role_name` varchar(20) NOT NULL COMMENT '角色名称',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `uk_role_code` (`role_code`) USING BTREE,
  UNIQUE KEY `uk_role_name` (`role_name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='角色表';

INSERT INTO `t_role` (`role_id`, `role_code`, `role_name`, `sort`) VALUES (1, 3, '超级管理员', 1);
INSERT INTO `t_role` (`role_id`, `role_code`, `role_name`, `sort`) VALUES (2, 2, '管理员', 2);
INSERT INTO `t_role` (`role_id`, `role_code`, `role_name`, `sort`) VALUES (3, 1, '普通用户', 3);

CREATE TABLE `t_role_auth` (
  `role_auth_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `auth_id` int(11) NOT NULL COMMENT '权限ID',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`role_auth_id`),
  UNIQUE KEY `uk` (`role_id`,`auth_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `t_role_auth` (`role_id`, `auth_id`) VALUES (1, 1);
INSERT INTO `t_role_auth` (`role_id`, `auth_id`) VALUES (1, 2);
INSERT INTO `t_role_auth` (`role_id`, `auth_id`) VALUES (1, 3);
INSERT INTO `t_role_auth` (`role_id`, `auth_id`) VALUES (1, 4);
INSERT INTO `t_role_auth` (`role_id`, `auth_id`) VALUES (1, 5);
INSERT INTO `t_role_auth` (`role_id`, `auth_id`) VALUES (1, 6);
INSERT INTO `t_role_auth` (`role_id`, `auth_id`) VALUES (1, 7);
INSERT INTO `t_role_auth` (`role_id`, `auth_id`) VALUES (1, 8);
INSERT INTO `t_role_auth` (`role_id`, `auth_id`) VALUES (1, 9);
INSERT INTO `t_role_auth` (`role_id`, `auth_id`) VALUES (1, 10);
INSERT INTO `t_role_auth` (`role_id`, `auth_id`) VALUES (1, 11);
INSERT INTO `t_role_auth` (`role_id`, `auth_id`) VALUES (1, 12);
INSERT INTO `t_role_auth` (`role_id`, `auth_id`) VALUES (1, 13);
INSERT INTO `t_role_auth` (`role_id`, `auth_id`) VALUES (1, 14);
INSERT INTO `t_role_auth` (`role_id`, `auth_id`) VALUES (1, 15);

CREATE TABLE `t_user` (
  `user_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL COMMENT '用户名',
  `password` varchar(255) NOT NULL COMMENT '密码',
  `nickname` varchar(20) DEFAULT NULL COMMENT '昵称',
  `avatar` varchar(100) DEFAULT NULL COMMENT '头像地址',
  `phone` varchar(20) DEFAULT NULL COMMENT '电话',
  `email` varchar(50) DEFAULT NULL COMMENT '用户邮箱',
  `sex` tinyint(4) DEFAULT '3' COMMENT '性别 1-男 2-女 3-未知',
  `disabled` tinyint(4) NOT NULL DEFAULT '0' COMMENT '禁用',
  `deleted` tinyint(4) NOT NULL DEFAULT '0' COMMENT '逻辑删除',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户表';

INSERT INTO `t_user` (`user_id`, `username`, `password`, `nickname`, `avatar`, `phone`, `email`, `sex`, `disabled`, `deleted`) VALUES (1, 'root', '$2a$10$nrX1Z3.cuByzUAlFh35VBOXeBXGuxYIfTCLGch/AkazwskrALC04C', '围巾', '', '15866666666', 'scarf@qq.com', 1, 0, 0);

CREATE TABLE `t_user_role` (
  `user_role_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `role_id` int(11) NOT NULL COMMENT '角色ID',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`user_role_id`),
  UNIQUE KEY `uk` (`user_id`,`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='用户角色关联表';

INSERT INTO `t_user_role` (`user_id`, `role_id`) VALUES (1, 1);
INSERT INTO `t_user_role` (`user_id`, `role_id`) VALUES (1, 2);
INSERT INTO `t_user_role` (`user_id`, `role_id`) VALUES (1, 3);

CREATE TABLE `t_menu` (
  `menu_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '菜单ID',
  `menu_type` tinyint(4) NOT NULL COMMENT '菜单类型 1-目录 2-菜单',
  `title` varchar(50) NOT NULL COMMENT '菜单名称',
  `icon` varchar(100) DEFAULT '#' COMMENT '菜单图标',
  `parent_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '父菜单ID',
  `name` varchar(50) DEFAULT '' COMMENT '路由名称',
  `path` varchar(255) NOT NULL COMMENT '路由地址',
  `query` varchar(255) DEFAULT '' COMMENT '路由参数',
  `component` varchar(255) DEFAULT NULL COMMENT '组件路径',
  `is_external` tinyint(4) DEFAULT '0' COMMENT '是否外部链接',
  `sort` int(4) DEFAULT '0' COMMENT '显示顺序',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='菜单表';


INSERT INTO `t_menu` (`menu_id`, `menu_type`, `title`, `icon`, `parent_id`, `name`, `path`, `query`, `component`, `is_external`, `sort`) VALUES (1, 1, '系统管理', 'system', 0, '', '/admin/system', NULL, 'Layout', 0, 1);
INSERT INTO `t_menu` (`menu_id`, `menu_type`, `title`, `icon`, `parent_id`, `name`, `path`, `query`, `component`, `is_external`, `sort`) VALUES (2, 2, '用户管理', 'user', 1, 'AdminUser', 'user', NULL, 'admin/system/user/index', 0, 1);
INSERT INTO `t_menu` (`menu_id`, `menu_type`, `title`, `icon`, `parent_id`, `name`, `path`, `query`, `component`, `is_external`, `sort`) VALUES (3, 2, '角色管理', 'peoples', 1, 'AdminRole', 'role', NULL, 'admin/system/role/index', 0, 2);
INSERT INTO `t_menu` (`menu_id`, `menu_type`, `title`, `icon`, `parent_id`, `name`, `path`, `query`, `component`, `is_external`, `sort`) VALUES (4, 2, '菜单管理', 'tree-table', 1, 'AdminMenu', 'menu', NULL, 'admin/system/menu/index', 0, 3);
INSERT INTO `t_menu` (`menu_id`, `menu_type`, `title`, `icon`, `parent_id`, `name`, `path`, `query`, `component`, `is_external`, `sort`) VALUES (5, 2, '字典管理', 'dict', 1, 'AdminDict', 'dict', '', 'admin/system/dict/index', 0, 4);
INSERT INTO `t_menu` (`menu_id`, `menu_type`, `title`, `icon`, `parent_id`, `name`, `path`, `query`, `component`, `is_external`, `sort`) VALUES (6, 1, '日志管理', 'log', 1, '', 'log', '', 'Middle', 0, 5);
INSERT INTO `t_menu` (`menu_id`, `menu_type`, `title`, `icon`, `parent_id`, `name`, `path`, `query`, `component`, `is_external`, `sort`) VALUES (7, 2, '操作日志', 'form', 6, 'OperLog', 'operlog', '', 'admin/system/log/operLog', 0, 1);
INSERT INTO `t_menu` (`menu_id`, `menu_type`, `title`, `icon`, `parent_id`, `name`, `path`, `query`, `component`, `is_external`, `sort`) VALUES (8, 2, '登录日志', 'logininfor', 6, 'LoginLog', 'loginlog', '', 'admin/system/log/loginLog', 0, 2);
INSERT INTO `t_menu` (`menu_id`, `menu_type`, `title`, `icon`, `parent_id`, `name`, `path`, `query`, `component`, `is_external`, `sort`) VALUES (9, 2, '巾帆迢迢', 'guide', 0, '', 'https://sscarf.com', NULL, '', 1, 99);

CREATE TABLE `t_operate_log` (
  `operate_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `module` varchar(255) DEFAULT NULL COMMENT '模块',
  `name` varchar(255) DEFAULT NULL COMMENT '操作名',
  `req_uri` varchar(255) DEFAULT NULL COMMENT '请求 uri',
  `req_method` varchar(255) DEFAULT NULL COMMENT '请求方法',
  `req_params` varchar(2000) DEFAULT NULL COMMENT '请求参数',
  `ip` varchar(255) DEFAULT NULL COMMENT '操作 ip',
  `address` varchar(255) DEFAULT NULL COMMENT '操作地址',
  `user_agent` varchar(255) DEFAULT NULL COMMENT 'User Agent',
  `operate_type` tinyint(4) DEFAULT NULL COMMENT '操作类型',
  `duration` bigint(20) DEFAULT NULL COMMENT '执行时长',
  `status` tinyint(4) DEFAULT NULL COMMENT '操作状态 0-失败 1-成功',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户 ID',
  `result_msg` varchar(2000) DEFAULT NULL COMMENT '返回结果',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`operate_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='操作日志';

CREATE TABLE `t_login_log` (
  `login_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL COMMENT '用户名',
  `ip` varchar(255) DEFAULT NULL COMMENT '登录 ip',
  `address` varchar(255) DEFAULT NULL COMMENT '登录地点',
  `user_agent` varchar(255) DEFAULT NULL COMMENT 'User Agent',
  `status` int(11) DEFAULT NULL COMMENT '登录状态 1-成功 0-失败',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '登录时间',
  PRIMARY KEY (`login_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='登录日志';